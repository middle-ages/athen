## Bibliographie

Amari, Michele: La guerra del vespro Siciliano o un periodo delle istorie Siciliane del secolo XIII, 2 Bde., Paris 1843 (dt.: Der sizilianische Vesperkrieg, Leipzig 1851)

Ammirato, Scipione: Istorie Fiorentine, hrsg. von F. Ranalli, 6 Bde., Florenz 1846–1849

Anselme de Sainte-Marie: Histoire généalogique et chronologique de la maison de France, 9 Bde., Paris 1726–1733

Arbois de Jubainville, Henry d': Voyage paléographique dans le département de l'Aube, Troyes/Paris 1855

Arbois de Jubainville, Henry d': Histoire des ducs et des comtes de Champagne, 6 Bde., Paris 1859–1869

Banduri, Anselm: Imperium Orientale sive antiquitates constantinopolitanae, 2 Bde., Venedig 1729

Baumeister, August: Topographische Skizze der Insel Euboia, Lübeck 1864

Bayet, Charles: Recherches pour servir à l'histoire de la peinture et de la sculpture chrétiennes en Orient, Paris 1879

Baumeister, August: Denkmäler des klassischen Altertums, 3 Bde., München/Leipzig 1885–1888

Bayet, Charles: L'Art Byzantin, Paris 1883

Bernhardy, G.: Grundriß der griechischen Literatur, 2 Bde., Halle 1836–1845 (²1852–1859; ³1861)

Beulé, Charles Ernest: L'Acropole d'Athènes, 2 Bde., Paris 1853–1854

Bikélas, Demetrius: Die Griechen des Mittelalters und ihr Einfluß auf die europäische Kultur, Gütersloh 1878

Bötticher, Adolf: Die Akropolis von Athen nach den Berichten der Alten und den neuesten Erforschungen, Berlin 1888

Bötticher, Karl: Bericht über die Untersuchungen auf der Akropolis von Athen im Frühjahr 1862, Berlin 1863

Bofarull, Antonio de: Crónica del Rey de Aragon D. Pedro IV, el Ceremonioso, Barcelona 1850

Bofarull, Antonio de: Historia critica de Cataluña, 9 Bde., Barcelona 1876–1878

Bohn, Richard: Die Propyläen der Akropolis zu Athen, Berlin/Stuttgart 1882

Bozzo, Stefano Vittorio: Note storiche Siciliane del secolo XIV, Palermo 1882

Brosset jeune: De la poésie géorgienne, Paris 1830

Brosset, Marie Félicité: Additions et éclaircissements à l'histoire de la Géorgie, Petersburg 1851

Buchon, J. A.: Notice sur un atlas en langue catalane de l'an 1374, Paris 1838

Buchon, J. A.: De l'etablissement d'une principauté française en Grèce après la 4e croisade, Paris 1842

Buchon, J. A.: La Grèce continentale et la Morée, Paris 1843

Buchon, J. A.: Nouvelles recherches historiques..., 2 Bde. mit Atlas, Paris 1843

Buchon, J. A.: Recherches historiques sur la principauté Française de Morée et ses hautes baronies, 2 Bde., Paris 1845

Buchon, J. A.: Histoire des Conquestes et de l'etablissement des Français dans les Etats de l'ancienne Grèce sous les Ville-Hardouin, Paris 1846

Burckhardt, Jakob: Die Zeit Konstantins des Großen, Basel 1853 (2. Aufl. Leipzig 1880)

Burnouf, Emile: La Ville et l'Acropole d'Athènes, Paris 1877

Bursian, Konrad: Geographie von Griechenland, 2 Bde., Leipzig 1862

Capmany, Antonio de: Memorias Historicas sobre la Marina... de Barcelona, 4 Bde., Madrid 1779–1792

Chandler, Richard: Travels in Greece or an account of a tour made at the expense of the Society of Dilettanti, Oxford 1776

Chappuis-Montlaville: Histoire du Dauphiné, 2 Bde., Paris 1827

Corlieu, A.: Les médecins Grecs depuis la mort de Galien jusqu'a la chute de l'empire d'Orient, Paris 1885

Cornelli, Vincenzo: Memorie istoriografiche delli regni della Morea, e Negroponte e luoghi adiacenti, Venedig 1686

Corsini, Odoardo: Fasti attici, 4 Bde., Florenz 1747–1764

Curtius, Ernst: Griechische Geschichte, 3 Bde., Berlin 1857 (51878–1880)

Datta, Pietro: Spedizione in oriente di Amedeo VI., conte di Savoia, Turin 1826

Datta, Pietro: Storia dei principi di Savoia del ramo d'Acqia signori del Piemonte, 2 Bde., Turin 1832

Delaville Le Roulx, J.: La France en Orient au XIVe siècle, 2 Bde., Paris 1886

De Rossi, Giovanni Battista: Piante iconografiche e prospettiche di Roma anteriori al secolo XVI, Rom 1879

Diehl, Charles: L'Église et les Mosaïques du Couvent de Saint-Luc en Phocide, Paris 1889

Diez, Friedrich: Leben und Werke der Troubadours, 2. verm. Aufl., Leipzig 1882

Dodwell, Edward. Klassische und topographische Reise nach Griechenland während des Jahres 1801, 2 Bde., Meiningen 1821

Dunlop, John: Geschichte der Prosadichtungen oder Geschichte der Romane, Novellen, Märchen u. s. w., Berlin 1851

Dunod, F. J.: Mémoires pour servir à l'histoire du Comté de Bourgogne, Besançon 1740

Egger, Emile: L'hellénisme en France, 2 Bde., Paris 1869

Ellissen, Adolf: Michael Akominatos von Chonä, Erzbischof von Athen, Nachrichten über sein Leben und seine Schriften, Göttingen 1846

Ellissen, Adolf: Zur Geschichte Athens nach dem Verluste seiner Selbständigkeit, Göttingen 1847

Ellissen, Adolf: Analekten der mittel- und neugriechischen Literatur, 5 Bde., Leipzig 1855–1862

Ersch, Johann Samuel/Johann Gottfried Gruber (Hrsg.): Allgemeine Encyclopädie der Wissenschaften und Künste, 167 Bde., Leipzig 1818–1889

Fabricius, Joannes Albertus: Bibliotheca Graeca, Hamburg 1790–1838

Fallmerayer, Jakob Philipp: Geschichte des Kaisertums Trapezunt, München 1827

Fallmerayer, Jakob Philipp: Geschichte der Halbinsel Morea während des Mittelalters. Ein historischer Versuch, Stuttgart/Tübingen 1830

Fallmerayer, Jakob Philipp: Welchen Einfluß hatte die Besetzung Griechenlands durch die Slaven auf die Schicksale der Stadt Athen, Stuttgart 1835

Fanelli, Francesco: Atene Attica descritta da suoi principii sino all' acquisto fatto dall' armi venete nel 1687 con varietà di medaglie, ritratti et disegni, Venedig 1707

Finlay, George: History of the Byzantine and Greek Empires from 1057 to 1453, London 1854

Finlay, George: The History of Greece under Ottoman and Venetian Domination (1453–1821), Edinburgh/London 1856

Finlay, George: A History of Greece from its conquest by the Romans to the present time, B. C. 146 to A. D. 1864, 7 Bde., Oxford 1877

Fischer, William: Studien zur byzantinischen Geschichte des 11. Jahrhunderts, Plauen 1883

Forchhammer, Peter Wilhelm: Hellenika, Griechenland im neuen das alte, Bd. 1, Berlin 1837

Freeman, Edward A.: The historical Geography of Europe, 2 Bde., London 1881

Gailhabaud, Jules: Monuments anciens et modernes, Paris 1850

Gardthausen, Viktor: Griechische Paläographie, Leipzig 1879

Gfrörer, Aug. Fr.: Byzantinische Geschichten, 3 Bde., Graz 1872

Gibbon, Edward: The history of the decline and fall of the Roman Einpire, 8 Bde., Paris 1840

Gregorio Rosario: Considerazioni sopra la storia di Sicilia dai tempi Normanni sino ai presenti, 6 Bde., Palermo 1805–1816

Guichenon, Samuel: Histoire généalogique de la maison de Savoie, 3 Bde., Paris 1660

Guillaume, Jean Baptiste: Histoire des sires de Salins, 2 Bde., Besançon 1757–1758

Guillet de Saint-George, Georges: Athènes ancienne et nouvelle et l'éstat présent de l'empire des Turcs... Avec le plan de la ville d'Athènes, Paris ³1676

Guldencrone, Diane de: L'Achaïe féodale, Étude sur le moyen-âge en Grèce (1205–1456), Paris 1886

Hammer-Purgstall, Joseph von: Geschichte des Osmanischen Reiches, 10 Bde., Pest 1827–1835

Hartwig, Otto: Die Übersetzungsliteratur Unteritaliens in der normannisch-staufischen Epoche, Leipzig 1886

Hermann, Karl Friedrich: Lehrbuch der griechischen Staatsaltertümer, Heidelberg ³1841

Hertzberg, Gustav Friedrich: Die Geschichte Griechenlands unter der Herrschaft der Römer, Halle 1866

Hertzberg, Gustav Friedrich: Geschichte Griechenlands, 4 Bde., Gotha 1876–1879

Hertzberg, Gustav Friedrich: Geschichte der Byzantiner und des osmanischen Reichs bis gegen Ende des XVI. Jahrhunderts, Berlin 1883

Heyd, Wilhelm: Geschichte des Levantehandels im Mittelalter, 2 Bde., Stuttgart 1879

Hilferding, Alexander: Geschichte der Serben und Bulgaren, 2 Bde., Bautzen 1856–1864

Hopf, Karl: Geschichtlicher Überblick über die Schicksale von Karystos, Sitzungsbericht der k.k. Akademie der Wissenschaften, Wien 1854

Hopf, Karl: Walther VI. von Brienne, Herzog von Athen und Graf von Lecce, in: Raumers Historisches Taschenbuch, Bd. 5, Leipzig 1854

Hopf, Karl: Dissertazione documentata sulla storia di Karystos nell' isola di Negroponte 1205–1470, Venedig 1856

Hopf, Karl: Veneto-Byzantinische Analecten, Wien 1859

Hopf, Karl: Geschichte Griechenlands vom Beginn des Mittelalters bis auf unsere Zeit, in: Ersch/Gruber (Hrsg.), Allgemeine Encyclopädie der Wissenschaften und Künste, Bd. 85, S. 67–465, Bd. 86, S. 1–90, Leipzig 1867, 1868

Hopf, Karl: Bonifaz von Montferrat, der Eroberer von Konstantinopel und der Troubadour Rambaut von Vaqueires, Berlin 1877

Hortis, Attilio: Studj sulle opere latine del Boccaccio, Triest 1879

Jahn, Otto: Aus der Altertumswissenschaft, Populäre Aufsätze, Bonn 1868

Jauna, Dominicus Ritter von: Histoire générale des roiaumes de Chypre, de Jerusalem..., 2 Bde., Leiden 1785

Jirecek, Joseph: Geschichte der Bulgaren, Prag 1876

Jourdain, Amable: Recherches critiques sur l'âge et l'origine des traductions Latines d'Aristote, nouv. ed., Paris 1843

Kállay, Benjamin von: Geschichte der Serben, 2 Bde., Budapest/Wien/Leipzig 1878

Kiepert, Heinrich: Lehrbuch der alten Geographie, Berlin 1878

Klaproth, Julius von: Reise in den Kaukasus und nach Georgien, 3 Bde., Halle/Berlin 1812–1814

Köhler, Gustav: Die Entwicklung des Kriegswesens und der Kriegsführung in der Ritterzeit, 3 Bde., Breslau 1886–1889 (Registerbde. 1890, 1893)

Krause, Johann Heinrich: Geographie Griechenlands, in: Ersch/Gruber (Hrsg.): Allgemeine Encyclopädie der Wissenschaften und Künste, Bd. 83, Leipzig 1866, S. 257–444

Krause, Johann Heinrich: Die Byzantiner des Mittelalters in ihrem Staats-, Hof- und Privatleben, Halle 1869

Krumbacher, Karl: Griechische Reise, Blätter aus dem Tagebuche einer Reise in Griechenland und in der Türkei, Berlin 1886

Kuhn, Emil: Die städtische und bürgerliche Verfassung des Römischen Reiches bis auf die Zeiten Justinians, Leipzig 1864–1865

Kunstmann, Friedrich: Studien über Marino Sanudo den Älteren, München 1855

Laborde, Léon: Athènes aux XVe, XVIe et XVIIe siècles, 2 Bde., Paris 1852

La Lumia, Isidoro: Frammento di studj storici sul secolo XIV in Sicilia, Palermo 1859

La Lumia, Isidoro: Storie Siciliane, 4 Bde., Palermo 1881–1883

Lambros, Spiridon: Μιχαὴλ ’Ακομινάτου του̃ Χονιάτου τὰ σωζόμενα, 2 Bde., Athen 1879–1880

Lambros, Spiridon: ‛Ιστορικὰ μελετήματα, Athen 1884

Lambros, Spiridon: ‛Ιστορία τη̃ς ‛Ελλάδος μετ' εικόνων απὸ τω̃ν αρχαιστάτων χρόνων μέχρι τη̃ς αλώσεως τη̃ς Κωνσταντινουπόλεως, 6 Bde., Athen 1885–1908

Landau, Markus: Giovanni Boccaccio, sein Leben und seine Werke, Stuttgart 1877

Lasaulx, Ernst von: Der Untergang des Hellenismus und die Einziehung seiner Tempelgüter durch die christlichen Kaiser, München 1854

Leake, William Martin: Travels in the Morea, 3 Bde., London 1830

Leake, William Martin: Travels in Northern Greece, 4 Bde., London 1835

Leake, William Martin: Topographie Athens, Zürich 1844

Leake, William Martin: Peloponnesiaca: a supplement to Travels in the Morea, London 1846

Lebeau, C.: Histoire du Bas-Empire, 21 Bde., Paris 1824–1836

Le Bret, Johann Friderich: Staatsgeschichte der Republik Venedig, 4 Bde., Riga 1769–1777

Legrand, Émile: Bibliothèque grecque vulgaire, Bd. 1–4, Paris 1880–1888 (Bd. 5–10, Paris 1890–1913)

Legrand, Émile: Bibliographie Hellénique, Bd. 1–2, Paris 1885 (Bd. 3–4, Paris 1903–1906)

Lelewel, Joachim: Geographie du Moyen âge, 4 Bde., Breslau

Lenormant, François: Recherches archéologiques à Éleusis, Paris 1862

Lenormant, François: La Grande-Grèce paysages et histoire, 3 Bde., Paris 1881

Le Quien, Michael: Oriens Christianus, in quatuor patriarchatus digestus, 3 Bde., Paris 1740

Leyser, Polykarp: Historia poetarum et poematum medii aevi, Halle 1721

Litta, Pompeo: Famiglie celebri italiane, 9 Bde., Mailand 1819–1883

Lunzi, Hermann: Della condizione delle isole Ionie, Venedig 1858

Mahn, C. A. F.: Die Werke der Troubadours, 4 Bde., Berlin 1846–1880

Mas Latrie, Louis de: Histoire de l'île de Chypre sous le regne des princes de la maison de Lusignan, 3 Bde., Paris 1852–1861

Mas Latrie, Louis de: Les princes de Morée ou d'Achaie 1203–1461, Venedig 1882

Maßmann, Hans Ferdinand: Der Kaiser und der Kunige buoch oder die sogen. Kaiserchronik, Gedicht des 12. Jahrh., 3 Bde., Quedlinburg 1849–1854

Mayer, Gustav: Essays und Studien zur Sprachgeschichte und Volkskunde, Berlin 1885

Mazzuchelli, Giammaria: Giunte all' opera, Gli Scrittori d'Italia, Rom 1884

Meliarakis, Antonios: ‛Υποήματα περιγραφικὰ τω̃ν Κυκλάδων νήσων, Athen 1880

Michaelis, Adolf: Der Parthenon, 2 Bde., Leipzig 1871

Miklosich, Franz: Die slavischen Elemente im Neugriechischen, Sitzungsbericht der k.k. Akademie der Wissenschaften, Wien 1869

Minieri Riccio, Camillo: Della dominazioni Angioina nel reame di Sicilia, Studii storici estratti da' registri della cancelleria Angioina di Napoli, Neapel 1876

Minieri Riccio, Camillo: Saggio di Codice diplomatico, 3 Bde., Neapel 1878

Moncada, Francisco de: Expedición de los Catalanes y Aragoneses contre Turcos y Griegos (Barcelona 1653), Madrid 1805

Moland, Louis: Saint-Omer dans la Morée, Esquisse de la Domination française dans la Grèce, au Moyen-Age, Paris 1855

Mommsen, August: Athenae Christianae, Leipzig 1868

Mortreuil, J. A. B.: Histoire du Droit byzantin, 3 Bde., Paris 1843–1847

Müller, August: Die griechischen Philosophen in der arabischen Überlieferung, Halle 1873

Müller, Johannes: Documenti sulle relazioni delle città Toscane coll'oriente, Florenz 1879

Müller, Joseph: Byzantinische Analekten aus der Handschrift der S. Markus-Bibliothek zu Venedig, Wien 1852

Müller, Karl Otfried: Orchomenos und die Minoer, Breslau 1820, 41844

Murray, John: Handbook for Travellers in Greece, London 1872

Mustoxydes, Andreas: Delle cose Corciresi, Bd. 1, Korfu 1848

Neumann, Karl: Griechische Geschichtsschreiber und Geschichtsquellen im 12. Jahrhundert, Leipzig 1888

Nicolai, Rudolf: Griechische Literaturgeschichte, 3 Bde., Magdeburg 1873

Ochoa, Eugenis de: Catálogo razonado de los manuscritos españoles existentes en la biblioteca real de Paris, Paris 1844

Ow, Josef: Die Abstammung der Griechen und die Irrtümer und Täuschungen des Dr. Ph. Fallmerayer, München 1847

Pallmann, Reinhold: Die Geschichte der Völkerwanderung von der Gotenbekehrung bis zum Tod Alarichs, Gotha 1863

Paparregopulos, Konstantin: ‛Ιστορία του̃ ‛Ελληνικου̃ έθνους απὸ τω̃ν αρχαιοτάτων χρόνων μέχρι τω̃ν νεωτέρων, 5 Bde., Athen 1860–1877

Paparregopulos, Konstantin: Histoire de la civilisation hellénique, Paris 1878

Parisot, Ernest: Histoire de la soie, 2 Bde., Paris 1862 ff.

Peruzzi, S. L.: Storia del Commercio e dei Banchieri di Firenze, Florenz 1868

Petersen, Friedrich Christian: Einleitung in das Studium der Archäologie, Leipzig 1829

Pittakis, K. L.: L'ancienne Athènes, ou la Description des antiquités d'Athènes et de ses environs, Athen 1835

Plancher, Urbain: Histoire générale et particulière de Bourgogne, 4 Bde., Dijon 1739–1781

Pouqueville, François: Voyage de la Grèce, 6 Bde., Paris 1826–1827

Prutz, Hans: Kulturgeschichte der Kreuzzüge, Berlin 1883

Rambaud, A.: L'Empire grec au Xe siècle, Paris 1870

Rangabe, Jakob R.: Τὰ ‛Ελληνικά, 3 Bde., Athen 1853

Riant, Paul: Expéditions et pélerinages des Scandinaves en Terre Sainte au temps des croisades, Paris 1865

Riant, Paul: Exuviae sacrae Constantinopolitanae, 3 Bde., Genf 1877 ff.

Röhricht, R./Heinrich Meisner: Deutsche Pilgerreisen nach dem heiligen Lande, Berlin 1880

Romanin, Samuele: Storia documentata di Venezia, 10 Bde., Venedig 1853–1861

Roß, Ludwig: Urkunden zur Geschichte Griechenlands im Mittelalter, München 1837

Roß, Ludwig: Die Demen von Attika und ihre Verteilung unter die Phylen, hrsg. von M. H. E. Meier, Halle 1846

Roß, Ludwig: Reisen des Königs Otto und der Königin Amalia in Griechenland, 2 Bde., Halle 1848

Roß, Ludwig: Archäologische Aufsätze, 2 Bde., Leipzig 1855–1861

Rubió y Lluch, Antonio: La expedición y dominación de los Catalanos en Oriente jusgadas por los Griegos, Barcelona 1883

Rubió y Lluch, Antonio: Los Navarros en Grecia y el ducado Catalán de Atenas en la época de su invasión, Barcelona 1886

Rubió y Lluch, Antonio: Don Guillermo Ramón Moncada, gran senescal de Cataluña, Barcelona 1886

Safarik, Paul Josef: Slavische Altertümer, hrsg. von Heinrich Wuttke, 2 Bde., Leipzig 1843–1844

Saint-Genois de Grandbreucq, François: Droits primitifs des anciennes terres, 1. Bd., Paris 1782

Sassenay, Fernand de: Les Brienne de Lecce et d' Athènes, Paris 1869

Sathas, Konstantin: Τουρκοκρατουμένη ‛Ελλάς 1453-1821, Athen 1869

Saulcy, Félicien de: Numismatique de Croisades, Paris 1874

Schaefer, Heinrich: Geschichte von Spanien, Gotha 1830 (= F. W. Lembke, H. Schaefer, Fr. W. Schirrmacher: Geschichte der europäischen Staaten, Bd. 2–3)

Schlumberger, Gustave: Numismatique de l'Orient latin, Paris 1878

Schlumberger, Gustave: Sigillographie de l'Empire byzantin, Paris 1884

Schmidt, Bernhard: Das Volksleben der Neugriechen und das Hellenische Altertum, Leipzig 1871

Schmidt, E.A.: Geschichte Aragoniens im Mittelalter, Leipzig 1828

Schmitt, John: Die Chronik von Morea, Eine Untersuchung über das Verhältnis ihrer Handschriften und Versionen zueinander, München 1889

Sieber, F.W.: Reise nach der Insel Kreta, 2 Bde., Leipzig 1823

Sievers, Gottlob Reinhold: Aus dem Leben des Libanius, Hamburg 1863

Sievers, Gottlob Reinhold: Studien zur Geschichte der Römischen Kaiser, Berlin 1870

Spon, Jakob: Relation de l'état présent de la ville d'Athènes, ancienne capitale de la Grèce, bâtie depuis 3400 ans, Lyon 1674

Spon, Jakob: Lettres écrites sur une dissertation d'un voyage de Grèce, hrsg. von Georges Guillet de Saint-George, Paris 1679

Spon, Jakob/Georgius Wheler: Italienische, Dalmatische, Griechische und Orientalische Reise-Beschreibung, 2 Bde., Nürnberg 1681

Stamatiadis, Epaminondas: Οι Καταλανοί εν τη̃ ’Ανατολη̃, Athen 1869

Streit, Ludwig: Venedig und die Wendung des vierten Kreuzzuges gegen Konstantinopel, Anklam 1877

Summonte, Antonio Giovanni: Istoria Napoli, 4 Bde., Neapel 1675

Surmelis, Dionysios: Κατάστασις συνοπτικὴ τη̃ς πόλεως ’Αθηνω̃ν, Athen 1842

Surmelis, Dionysios: Αττικὰ ὴ περὶ τω̃ν Δη̃μων ’Αττικη̃ς, Athen 1855

Sybel, Ludwig von: Katalog der Skulpturen zu Athen, Marburg 1881

Tafel, Gottlieb L. F.: De Thessalonica, Tübingen 1839

Tafel, Gottlieb L. F.: Symbolarum criticarum, geographiam Byzantinam spectantium, 2 Bde., München 1848–1849

Tafel, Gottlieb L. F./Georg Martin Thomas: Urkunden zur Handels- und Staatsgeschichte der Republik Venedig, 3 Bde., Wien 1856–1857

Tanfani, Leopoldo: Niccola Acciainoli, studi storici, Florenz 1803

Targioni-Tozzetti, Giovanni: Relazioni d'alcuni viaggi fatti in diverse parti della Toscana, 12 Bde., Florenz 1768–1779

Testa, F.: De vita et rebus gestis Frederici II. Siciliae regis, Palermo 1775

Thomas, Georg Martin: Diplomatarium Veneto-Levantinum 1300–1350, Venedig 1880

Tournefort, J. de: Relation d'un voyage au Levant, Paris 1717

Ullmann, C.: Gregorius von Nazianz, der Theologe, Gotha 1867

Ulrichs, Heinrich Nikolaus: Reisen und Forschungen in Griechenland, 2 Bde., Berlin 1840–1863

Unger, Friedrich Wilhelm: Christlich-griechische oder byzantinische Kunst, in: Ersch/Gruber (Hrsg.): Allgemeine Encyclopädie der Wissenschaften und der Künste, Bd. 84, S. 291–474, Bd. 85, S. 1–66, Leipzig 1866, 1867

Voigt, Georg: Enea Silvio de' Piccolomini, als Papst Pius der Zweite und sein Zeitalter, 3 Bde., Berlin 1856-1863

Wachsmuth, Kurt: Die Stadt Athen im Altertum, Bd. 1, Leipzig 1874 (Bd. 2, Leipzig 1890)

Weber, Karl Friedrich: De academia literaria Atheniensium seculo secundo post Chr. const., Marburg 1858

Wietersheim, Eduard von: Geschichte der Völkerwanderung, 4 Bde., Leipzig 1859–1864

Wilken, Friedrich: Geschichte der Kreuzzüge, 3 Bde., Leipzig 1807–1817

Zachariä von Lingenthal, Karl Eduard: Jus Graeco-Romanum, 7 Bde., Leipzig 1856–1884

Zachariä von Lingenthal, Karl Eduard: Geschichte des Griechisch-Römischen Privat-Rechtes, Leipzig 1864 (2. Aufl. u. d. Titel: Geschichte des griechisch-römischen Rechts, Berlin 1877, ³1892)

Zanelli, Agostino: Le schiave orientali a Firenze nei secoli XIV e XV, Florenz 1885

Zeller, Eduard: Die Philosophie der Griechen in ihrer geschichtlichen Entwicklung, 6 Bde., 1862–1882

Zeuss, Johann Kaspar: Die Deutschen und die Nachbarstämme, München 1837

Zinkeisen, Johann Wilhelm: Geschichte des osmanischen Reiches in Europa, 7 Bde., Hamburg 1840–1863

Zumpt, Karl Gottlob: Über den Bestand der philosophischen Schulen in Athen, Berlin 1843
