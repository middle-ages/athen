#### 5.

Der Verzicht auf seinen jahrelangen Verkehr mit den Redekünstlern und Gelehrten Konstantinopels, seinen geistreichen Freunden, konnte für den Erzbischof Athens nicht einmal durch das Bewußtsein gemildert werden, daß er sich auf der heiligsten Kulturstätte des klassischen Altertums befand. Denn der Anblick dieser Trümmer machte ihn schwermütig. Er klagte, daß von der Heliaia, vom Peripatos und dem Lykeion keine Spur mehr übrig geblieben sei; nur der Areopag strecke seine kahle Felsenfläche empor. Man zeigte noch geringe Reste, vom Zahn der Zeit zerfressene Bausteine der Stoa Poikile, wo Schafe weideten, wie zu Rom Vieh auf dem Forum und Ziegen auf dem Kapitol.τη̃ς ποικίλης Στοα̃ς μικρὸν λείψανον μηλόβοτον καὶ αυτὸ καὶ τοι̃ς οδου̃σι του̃ χρόνου τὰς πλίνθους παρατρωγόμενεν. Rede an Drimys I, p. 160.

Nur dies hatten die Athener vor den Römern voraus, daß die berühmtesten Schauplätze ihrer geschichtlichen Vergangenheit, wenn auch trümmerhaft, unkenntlich und sagenhaft geworden, doch nicht so tief herabgewürdigt waren, um ihre vornehmen und klassischen Namen in »Kuhfeld« und »Ziegenberg« zu verändern. Michael Akominatos durfte es den Athenern, wenn auch nicht ohne sie zu beschämen, zum Ruhme anrechnen, daß sie noch die Quelle Kallirrhoe, den Areopag und einige andere Orte mit ihren antiken Namen benannten.Die Kallirrhoe nennt er mehrmals II, p. 26, 44, 400. Auch den Kerameikos II, p. 238. In seinen Schriften sind die Berge des Pedion Athens außer dem Hymettos nicht bezeichnet; selbst der Lykabettos oder Anchesmos ist nicht genannt. Wenn er den Hymettos bestieg, wo noch heute das alte, schöne Kloster Kaisariani aufrecht steht, dessen entzückende Lage er wie ein Dichter geschildert hat, war er erfreut, ganz Attika, die Kykladen und Sporaden »wie auf einer Landkarte« zu übersehen und die Gestade, Ankerplätze und Häfen zu betrachten, die sich dort vor seinen Blicken ausbreiteten. Mit Genugtuung bemerkte er, daß die Eilande Psyttaleia, Salamis, Ägina noch »ihre alte Benennung« trugen.νήσους ω̃ν καὶ παρὰ τοι̃ς παλαίοις ονόματα. Vom Hymettoskloster II, p. 13, 14. Dies Gebirge, neugriechisch Trellobuni, nannten die Italiener Monte Matto; J. P. Rangabe, Τὰ ‛Ελληνικά, Athen 1853, I, p. 160. Ovid, Ars am. II, 687ff., hat den Abhängen des Hymettos ein paar Verse gewidmet. Demnach hatte auch die Insel Salamis noch nicht durchaus ihren Namen verloren, obwohl sie schon längst, vielleicht von ihrer Brezelgestalt, die volkstümliche Bezeichnung Kuluris trug.So in der fränkischen ›Partitio regni graeci‹ von 1204, Tafel u. Thomas I, p. 469; in der Note 5 wird der Scholiast zum Ptolem. 3, 16, 23 angezogen: Σαλαμίς, η Κουλούρι. Selbst der Name der kleinen, aus der Xerxeszeit berühmten Scholle Psyttaleia erhielt sich und dauert noch heute in der Umgestaltung Leipso Kutuli fort; die dort in der Nähe gelegene Felsenklippe Atalante heißt noch jetzt Talanto.Bursian I, p. 365f.

Leider hat Akominatos nur sehr wenige alte Denkmäler Athens namentlich erwähnt und von vielen geschwiegen, die noch zu seiner Zeit mehr oder minder erhalten waren oder noch nicht ganz von Pflanzendickicht und Schutt bedeckt sein konnten; wie das Theater des Dionysos, das Hadrianstor, das Olympieion, der Turm der Winde, das Odeon des Herodes, das Stadion, die Pnyx, das Museion mit dem Philopapposdenkmal, die Tempel, Gymnasien und Hallen an der neuen Agora, die Wasserleitung des Hadrian und Antonin und manche andere Überreste des Altertums, die wir noch heute bewundern. Von solchen Bauwerken, die jetzt spurlos sind, wie das Metroon, Prytaneion, das Rathaus der Fünfhundert, das Pompeion, Eleusinion, die Akademie, die Stoa Eleutherios und Basileios, die antiken Tore, hat er nichts gesagt. An viele Monumente Athens, wie an jene der Kaiserstadt Rom, hatte die Phantasie des Volks Legenden und Dichtungen angeheftet. Wenn der gelehrte Erzbischof in seiner Antrittsrede an die Athener das Denkmal des Lysikrates durchaus als Laterne des Demosthenes (ο Δημοσθένους λύχνος) bezeichnete, so sieht das so aus, als habe er selbst an diese fabelhafte Bestimmung jenes choregischen Ehrenmals geglaubt.Das Bestehen dieses Vulgärnamens schon im 12. Jh. beweist, wie irrig Fallmerayer ihn und andere den eingewanderten Albanesen zugeschrieben hat. (Welchen Einfluß etc., S. 51.) Vielleicht war es ebendies berühmte Bauwerk, welches die Alexandersage in einen hundert Fuß hohen Pfeiler mitten in Athen verwandelt hat; auf ihm brenne eine Lampe, von der die ganze Stadt erleuchtet werde; das aber sei ein Kunstwerk Platos. So erscheint der unsterbliche Philosoph als ein magischer Schutzherr Athens, wie es der Dichter Virgil für Rom und der weise Apollonios von Tyana für Konstantinopel gewesen sein sollten.

> En milieu de la ville ont drecié un piler,  
C. pies avoit de haut, Platons le fist lever;  
Deser ot une lampe, en sou i calender.

Manche andere Sagen von der Erbauung der Prachtmonumente Athens hatten sich längst ausgebildet. Wenn im ›Liber Guidonis‹ die Propyläen als ein vom alten Heros Jason gegründeter Tempel bezeichnet werden, so läßt das auf einen griechischen Sagenkreis schließen, der für uns untergegangen ist.

Der Anblick der Ruinen Athens regte die dichterische Phantasie des Schülers des Eustathios zu einer jambischen Elegie auf. Sie ist die erste und auch die einzige Klagestimme über den Untergang der alten, erlauchten Stadt, welche auf uns gekommen ist.

> Die Liebe zu Athen, des Ruhm einst weit erscholl,  
Schrieb dieses nieder, doch mit Wolken spielt sie nur,  
Und kühlt an Schatten ihrer Sehnsucht heiße Glut.  
Denn nimmer, ach! und nirgend mehr erschaut mein Blick  
Hier jene einst im Lied so hochgepriesne Stadt.  
In der Äonen Lauf hat ungemeßne Zeit  
Sie tief begraben unter Steingeröll und Schutt,  
Und so erduld ich hoffnungsloser Sehnsucht Qual.  
Wem es versagt ist, mit dem Blick der Gegenwart  
In voller Wirklichkeit Geliebtes anzuschaun,  
Der lindert etwa doch mit holdem Schein die Glut  
Der Liebe, wenn das Bild des Freundes er erblickt.  
Doch dem Ixion bin Unseliger ich gleich  
So lieb Athen ich auch wie Hera er geliebt,  
Und dann ihr glanzvoll Schattenbildnis hat umarmt.  
Weh mir! was leid und sag ich, und was schreib ich hier!  
Athen bewohn ich und doch schau ich nicht Athen,  
Nur öde Herrlichkeit bedeckt mit grausem Schutt.  
O Stadt des Jammers, wo sind deine Tempel hin?  
Wie ward zunicht hier alles, schwand zur Sage fort  
Gericht und Richter, Rednerbühne, Abstimmung,  
Gesetze, Volksversammlung und des Redners Kraft,  
Der Rat, der Feste heilger Pomp und der Strategen  
Kriegsherrlichkeit im Kampf zu Lande wie zur See,  
Die formenreiche Muse und der Denker Macht.  
Ein Untergang verschlang den ganzen Ruhm Athens,  
Kein Pulsschlag lebt davon, kein kleinstes Merkmal nur.Den Threnos veröffentlichte zuerst Boissonade, Anec. Gr. V, p. 374; dann Ellissen, Monographie über M. Alt.; Lambros II, p. 397. Er erscheint wie die dichterische Einleitung zu einer vielleicht antiquarischen Schrift über die Monumente Athens, die nicht ausgeführt worden ist. Akominatos schickte die Verse an seine Freunde, so an Georg Tornikis, den Metropoliten von Ephesos, der ihm dafür dankte (II, p. 412).

Parallelen zu diesem Klageliede sind die Elegien, welche Lateiner dem Falle der großen Roma gewidmet haben. Unter diesen ist die ergreifendste jene des gallischen Bischofs Hildebert von Tours, eins der schönsten lateinischen Gedichte des Mittelalters. Hildebert sah und beklagte die zertrümmerte Weltstadt nach ihrer Verheerung durch die Normannen Guiscards im Jahre 1106. Sein Klagegesang beginnt mit den Versen:

> Nichts ist, Roma, dir gleich, selbst jetzt, da du moderst in Trümmern,  
Was in dem Glanze du warst, lehren Ruinen im Staub.  
Deine prangende Größe zerstörte die Zeit, und es liegen  
Kaiserpaläste und auch Tempel der Götter im Sumpf...

Diese Elegien zweier Bischöfe der lateinischen und griechischen Kirche, welche Glaubensspaltung feindlich getrennt hielt, sind den zwei Hauptstädten der antiken Weltkultur geweiht. Beide gehören, das eine dem Anfange, das andere dem Ende desselben Jahrhunderts an, und beiden gibt derselbe melancholische Gegenstand eine ergreifende Übereinstimmung.

Die Klagestimmen über den Untergang Roms sind durch alle Zeiten fortgesetzt worden, aber die Trümmer Athens haben seit Akominatos keine Elegie eines Griechen mehr hervorgerufen, oder eine solche ist uns unbekannt geblieben. Nur bei einzelnen Dichtern der Renaissance, wie in einer Ode des Markos Musuros auf Platon, ferner im Threnos des Korfioten Antonios Eparchos über Griechenland und in dem Gedichte ›Hellas‹ des Chioten Leon Allatios zur Zeit des Papstes Urban VIII., wird Athens im allgemeinen Untergange des hellenischen Landes gedacht.Die Verse des Musuros bei C. Sathas, Neohell. Philol., p. 89. Ibid., p. 165, der Threnos des Eparchos. Die Verse des Allatios in: Natales Delphini Gallici, Rom 1692. Ein Fragment einer Klage über Athen schickte Theod. Zygomalas an Crusius 1581 (Turcograecia, p. 95)
