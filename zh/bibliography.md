## 参考书目

Amari, Michele: La guerra del vespro Siciliano o un periodo delle istorie Siciliane del secolo XIII, 2 vols., Paris 1843 (Eng.: Der sizilianische Vesperkrieg, Leipzig 1851)

Ammirato, Scipione: Istorie Fiorentine, ed. by F. Ranalli, 6 vols.

Anselme de Sainte-Marie: Histoire généalogique et chronologique de la maison de France, 9 vols, Paris 1726-1733

Arbois de Jubainville，Henry d'：Voyage paléographique dans le département de l'Aube，特鲁瓦/巴黎，1855 年

Arbois de Jubainville，Henry d'：《香槟公爵和伯爵史》，6 卷，巴黎，1859-1869 年

安塞尔姆-班杜里：Imperium Orientale sive antiquitates constantinopolitanae，2 卷，威尼斯，1729 年

奥古斯特-鲍迈斯特：《欧波亚岛地形简图》，吕贝克，1864 年

巴耶特、查尔斯：《为东方基督教绘画和雕塑史服务的研究》，巴黎，1879 年

奥古斯特-鲍迈斯特：《古典古代艺术》，3 卷，慕尼黑/莱比锡，1885-1888 年

巴耶特、查尔斯：《拜占庭艺术》，巴黎，1883 年

Bernhardy, G.: Grundriß der griechischen Literatur, 2 vols.

Beulé，Charles Ernest：L'Acropole d'Athènes，2 卷，巴黎，1853-1854 年

Bikélas，Demetrius：Die Griechen des Mittelalters und ihr Einfluß auf die europäische Kultur，Gütersloh 1878 年

Bötticher，Adolf：Die Akropolis von Athen nach den Berichten der Alten und den neuesten Erforschungen，柏林，1888 年

Bötticher，Karl：《1862 年春雅典卫城调查报告》，柏林，1863 年

Bofarull, Antonio de： Crónica del Rey de Aragon D. Pedro IV, el Ceremonioso, Barcelona 1850

Bofarull, Antonio de： Historia critica de Cataluña, 9 vols, Barcelona 1876-1878

Bohn、Richard：Die Propyläen der Akropolis zu Athen，柏林/斯图加特，1882 年

Bozzo，Stefano Vittorio：Note storiche Siciliane del secolo XIV，巴勒莫，1882 年

年轻的布罗塞：《格鲁吉亚诗歌》，巴黎，1830 年

Brosset, Marie Félicité：Additions et éclaircissements à l'histoire de la Géorgie，彼得堡，1851 年

Buchon, J. A.: Notice sur un atlas en langue catalane de l'an 1374, Paris 1838

Buchon, J. A.: De l'etablissement d'une principauté française en Grèce après la 4e croisade, Paris 1842

Buchon, J. A.: La Grèce continentale et la Morée, Paris 1843

Buchon，J. A.：Nouvelles recherches historiques...，2 卷，附地图集，巴黎，1843 年

Buchon, J. A.: Recherches historiques sur la principauté Française de Morée et ses hautes baronies, 2 vols.

Buchon, J. A.: Histoire des Conquestes et de l'etablissement des Français dans les Etats de l'ancienne Grèce sous les Ville-Hardouin, Paris 1846.

雅各布-布克哈特：Die Zeit Konstantins des Großen，巴塞尔，1853 年（第 2 版，莱比锡，1880 年）

Burnouf, Emile： La Ville et l'Acropole d'Athènes》，巴黎，1877 年

康拉德-布尔西安：《希腊地理》，第 2 卷，莱比锡，1862 年

Capmany, Antonio de： Memorias Historicas sobre la Marina... de Barcelona》，4 卷，马德里，1779-1792 年

理查德-钱德勒：《希腊游记或由 Dilettanti 协会出资的游记》，牛津，1776 年

Chappuis-Montlaville: Histoire du Dauphiné，2 卷，巴黎，1827 年

Corlieu，A.：Les médecins Grecs depuis la mort de Galien jusqu'a la chute de l'empire d'Orient，巴黎，1885 年

Cornelli，Vincenzo：Memorie istoriografiche delli regni della Morea，e Negroponte e luoghi adiacenti，威尼斯，1686 年

科尔西尼、奥多亚多：《傲慢与偏见》（Fasti attici），4 卷，佛罗伦萨，1747-1764 年

恩斯特-库尔提乌斯：《希腊史》，3 卷，1857 年柏林（51878-1880 年）

Datta，Pietro：Spedizione in oriente di Amedeo VI，conte di Savoia，都灵 1826 年

Datta，Pietro：Storia dei principi di Savoia del ramo d'Acqia signori del 皮埃蒙特，2 卷，都灵，1832 年

Delaville Le Roulx, J.: La France en Orient au XIVe siècle, 2 vols.

De Rossi、Giovanni Battista：Piante iconografiche e prospettiche di Roma anteriori al secolo XVI，罗马 1879 年

Diehl，Charles：L'Église et les Mosaïques du Couvent de Saint-Luc en Phocide，巴黎，1889 年

Diez, Friedrich：Leben und Werke der 吟游诗人，第二次修订版，莱比锡，1882 年

多德维尔，爱德华。1801 年希腊古典与地形之旅》，第 2 卷，迈宁根，1821 年

Dunlop, John: Geschichte der Proadichtungen oder Geschichte der Romane, Novellen, Märchen u. s. w., Berlin 1851

Dunod, F. J.: Mémoires pour servir à l'histoire du Comté de Bourgogne, Besançon 1740

Egger, Emile： L'hellénisme en France》，2 卷，巴黎，1869 年

Ellissen，Adolf：Michael Akominatos von Chonä，Archbishop of Athens，Nachrichten über sein Leben und seine Schriften，哥廷根，1846 年

Ellissen，Adolf：On the history of Athens after the loss of its independence，哥廷根，1847 年

阿道夫-埃利森：《中近代希腊文学论语》，5 卷，莱比锡，1855-1862 年

Ersch, Johann Samuel/Johann Gottfried Gruber (ed.): Allgemeine Encyclopädie der Wissenschaften und Künste, 167 vols.

法布里齐乌斯、约安内斯-阿尔贝特斯：《希腊百科全书》，汉堡 1790 年至 1838 年

法尔梅拉耶、雅各布-菲利普：《特雷比曾帝国史》，慕尼黑 1827 年

法尔梅拉耶、雅各布-菲利普：中世纪莫雷亚半岛史。历史尝试，斯图加特/图宾根，1830 年

Fallmerayer，Jakob Philipp：斯拉夫人占领希腊对雅典城的命运有何影响，斯图加特，1835 年

Fanelli，Francesco：Atene Attica descritta da suoi principii sino all' acquisto fatto dall' armi venete nel 1687 con varietà di medaglie, ritratti et disegni，威尼斯，1707 年

乔治-芬利：《1057 年至 1453 年拜占庭和希腊帝国史》，伦敦，1854 年

芬利、乔治：《奥斯曼帝国和威尼斯统治下的希腊史（1453-1821 年）》，爱丁堡/伦敦，1856 年

Finlay，George：A History of Greece from its conquest by the Romans to the present time, B. C. 146 to A. D. 1864, 7 vols.

Fischer, William: Studien zur byzantinischen Geschichte des 11.

Forchhammer, Peter Wilhelm: Hellenika, Griechenland im neuen das alte, vol. 1, Berlin 1837

弗里曼、爱德华-A.：《欧洲历史地理》，第 2 卷，伦敦，1881 年

朱尔斯-盖尔哈博：《古代和现代遗迹》，巴黎，1850 年

Gardthausen，Viktor：Griechische Paläographie，Leipzig 1879 年

Gfrörer，Aug. Fr.：《拜占庭历史》，3 卷，格拉茨，1872 年

吉本、爱德华：《罗马帝国衰亡史》，8 卷，巴黎，1840 年

格雷戈里奥-罗萨里奥：Considerazioni sopra la storia di Sicilia dai tempi Normanni sino ai presenti，6 卷，巴勒莫，1805-1816 年

Guichenon，Samuel：Histoire généalogique de la maison de Savoie，3 卷，巴黎，1660 年

让-巴蒂斯特-纪尧姆： 萨林斯家族史》，2 卷，贝桑松，1757-1758 年

Guillet de Saint-George，Georges：Athènes ancienne et nouvelle et l'éstat présent de l'empire des Turcs... Avec le plan de la ville d'Athènes, Paris ³1676

古尔登克隆，戴安娜-德： L'Achaïe féodale, Étude sur le moyen-âge en Grèce (1205-1456), Paris 1886

约瑟夫-冯-哈默-普格斯托尔 奥斯曼帝国史》，10 卷，佩斯，1827-1835 年

Hartwig, Otto：Die Übersetzungsliteratur Unteritaliens in der normannisch-staufischen Epoche，莱比锡，1886 年

赫尔曼、卡尔-弗里德里希：Lehrbuch der griechischen Staatsaltertümer，海德堡，1841 年

赫茨贝格、古斯塔夫-弗里德里希：Die Geschichte Griechenlands unter der Herrschaft der Römer，哈雷 1866 年

赫茨伯格、古斯塔夫-弗里德里希：《希腊史》，4 卷，哥达，1876-1879 年

Hertzberg, Gustav Friedrich: Geschichte der Byzantiner und des osmanischen Reichs bis gegen Ende des XVI. Jahrhunderts, Berlin 1883

海德、威廉：《中世纪黎凡特贸易史》，2 卷，斯图加特，1879 年

亚历山大-希尔费丁：《塞尔维亚人和保加利亚人史》，2 卷，包岑，1856-1864 年

Hopf, Karl: Geschichtlicher Überblick über die Schicksale von Karystos, Sitzungsbericht der k.k.. 科学院，维也纳，1854 年

Hopf, Karl: Walther VI of Brienne, Duke of Athens and Count of Lecce, in: Raumers Historisches Taschenbuch, vol. 5, Leipzig 1854

Hopf, Karl：Dissertazione documentata sulla storia di Karystos nell' isola di Negroponte 1205-1470，威尼斯，1856 年

卡尔-霍普夫：《威尼斯-拜占庭论语》，维也纳，1859 年

Hopf, Karl: Geschichte Griechenlands vom Beginn des Mittelalters bis auf unsere Zeit, in: Ersch/Gruber (ed.), Allgemeine Encyclopädie der Wissenschaften und Künste, vol. 85, pp.

Hopf, Karl: Boniface of Montferrat, the Conqueror of Constantinople and the Troubadour Rambaut of Vaqueires, Berlin 1877 年

Hortis，Attilio：Studj sulle opere latine del Boccaccio，的里雅斯特 1879 年

Jahn, Otto: Aus der Altertumswissenschaft, Populäre Aufsätze, Bonn 1868

Jauna, Dominicus Ritter von： Histoire générale des roiaumes de Chypre, de Jerusalem..., 2 vols, Leiden 1785

约瑟夫-伊雷切克：《保加利亚人史》，布拉格，1876 年

Jourdain，Amable：Recherches critiques sur l'âge et l'origine des traductions Latines d'Aristote, nouv. ed., Paris 1843

卡莱，本杰明-冯： 塞尔维亚人史》，2 卷，布达佩斯/维也纳/莱比锡，1878 年

海因里希-基佩特：《古代地理教科书》，柏林，1878 年

Klaproth, Julius von： Reise in den Kaukasus und nach Georgien, 3 vols., Halle/Berlin 1812-1814

Köhler, Gustav: Die Entwicklung des Kriegswesens und der Kriegsführung in der Ritterzeit, 3 vols., Breslau 1886-1889 (Registerbde. 1890, 1893)

Krause, Johann Heinrich: Geography of Greece（《希腊地理》），载于 Ersch/Gruber (ed.)：Allgemeine Encyclopädie der Wissenschaften und Künste，第 83 卷，莱比锡，1866 年，第 257-444 页。

Krause、Johann Heinrich：《中世纪拜占庭人的国家、宫廷和私人生活》，哈雷，1869 年

Krumbacher，Karl：Griechische Reise，Blätter aus dem Tagebuche einer Reise in Griechenland und in der Türkei，柏林，1886 年

Kuhn，Emil：Die städtische und bürgerliche Verfassung des Römischen Reiches bis auf die Zeiten Justinians，莱比锡，1864-1865 年

Kunstmann，Friedrich：《老马里诺-萨努多研究》，慕尼黑，1855 年

Laborde，Léon：《十五、十六和十七世纪的阿泰斯》，2 卷，巴黎，1852 年

La Lumia，Isidoro：Frammento di studj storici sul secolo XIV in Sicilia，巴勒莫，1859 年

伊西多罗-拉卢米亚：《西西里故事》，4 卷，巴勒莫，1881-1883 年

Lambros，Spiridon：《Μιχαὴλ 'Ακομινάτου του̃ Χονιάτου τὰσωζόμενα》，2 卷，雅典，1879-1880 年

Lambros，Spiridon：《Иστορικὰ μελετήματα》，雅典 1884 年

Lambros, Spiridon: ‛Ιστορία τη̃ς Ελλλάδος μετ' εικόνων απὸ τω̃ν αρχαιστάτων χρόνων μέχρι τη̃ς αλώσεως τη̃ς Κωνσταντινουπόλεως, 6 vols, Athens 1885-1908

Landau，Markus：《乔瓦尼-薄伽丘，他的生平和作品》，斯图加特，1877 年

Lasaulx, Ernst von： Der Untergang des Hellenismus und die Einziehung seiner Tempelgüter durch die christlichen Kaiser，慕尼黑，1854 年

威廉-马丁-利克：《莫雷亚游记》，3 卷，伦敦，1830 年

威廉-马丁-利克：《希腊北部游记》，4 卷，1835 年伦敦版

利克、威廉-马丁：《雅典地形图》，苏黎世，1844 年

李克、威廉-马丁：《伯罗奔尼撒：莫雷亚游记补编》，伦敦，1846 年

Lebeau, C.：《下帝国史》，21 卷，巴黎，1824-1836 年

勒布雷特、约翰-弗里德里希：《威尼斯共和国国家史》，4 卷，1769-1777 年，里加

Legrand, Émile： Legrand, Émile: Bibliothèque grecque vulgaire, vols. 1-4, Paris 1880-1888 (vols. 5-10, Paris 1890-1913)

Legrand, Émile： Bibliographie Hellénique, vols. 1-2, Paris 1885 (vols. 3-4, Paris 1903-1906)

Lelewel, Joachim: Geographie du Moyen âge, 4 vols.

Lenormant，François：Recherches archéologiques à Éleusis，巴黎，1862 年

Lenormant，François：《La Grande-Grèce paysages et histoire》，3 卷，1881 年，巴黎

Le Quien，Michael：Oriens Christianus，in quatuor patriarchatus digestus，3 卷，巴黎，1740 年

波利卡普-莱塞尔：《中世纪诗人和诗歌史》，哈勒 1721 年

Litta, Pompeo： 意大利名人家族》，9 卷，米兰，1819-1883 年

Lunzi，Hermann：Della condizione delle isole Ionie，威尼斯，1858 年

Mahn，C. A. F.：Die Werke der 吟游诗人，4 卷，柏林，1846-1880 年

Mas Latrie, Louis de： 路易-德-马斯-拉特里：《吕西尼昂王公统治下的茜普尔岛历史》，3 卷，巴黎，1852-1861 年

Mas Latrie, Louis de： Les princes de Morée ou d'Achaie 1203-1461, Venice 1882

Maßmann，Hans Ferdinand：Der Kaiser und der Kunige buoch oder die sogen. Kaiserchronik，12 世纪的诗歌，3 卷，奎德林堡，1849-1854 年

古斯塔夫-迈耶：《语言史和民俗学论文与研究》，柏林，1885 年

Mazzuchelli，Giammaria：Giunte all' opera，Gli Scrittori d'Italia，罗马，1884 年

Meliarakis、Antonios：《Υποήματα περιγραφικὰ τω̃ν Κυκλάδων νήσων 》，雅典，1880 年

迈克尔斯、阿道夫：《帕台农神庙》，2 卷，莱比锡，1871 年

Miklosich，Franz：Die slavischen Elemente im Neugriechischen，Sitzungsbericht der k.k.。科学院，维也纳，1869 年

Minieri Riccio, Camillo: Della dominazioni Angioina nel reame di Sicilia, Studii storici estratti da' registri della cancelleria Angioina di Napoli, Naples 1876 年

Minieri Riccio，Camillo：Saggio di Codice diplomatico，3 卷，那不勒斯，1878 年

Moncada, Francisco de： Expedición de los Catalanes y Aragoneses contre Turcos y Griegos (Barcelona 1653)，马德里，1805 年

Moland, Louis：Saint-Omer dans la Morée，Esquisse de la Domination française dans la Grèce，au Moyen-Age，Paris 1855

奥古斯特-莫姆森：《基督教雅典娜》，莱比锡，1868 年

Mortreuil, J. A. B.： Histoire du Droit byzantin, 3 vols, Paris 1843-1847

Müller, August: Die griechischen Philosophen in der arabischen Überlieferung, Halle 1873

Müller，Johannes：Documenti sulle relazioni delle città Toscane coll'oriente，佛罗伦萨，1879 年

Müller，Joseph：Byzantinische Analekten aus der Handschrift der S. Markus-Bibliothek zu Venedig，维也纳，1852 年

Müller, Karl Otfried: Orchomenos and the Minoans, Breslau 1820, 41844

约翰-默里：《希腊旅行者手册》，伦敦，1872 年

Mustoxydes，Andreas：《Delle cose Corciresi》，第 1 卷，科孚岛，1848 年

Neumann, Karl: Griechische Gesichtsschreiber und Geschichtsquellen im 12.

尼古拉、鲁道夫：《希腊文学史》，3 卷，马格德堡，1873 年

Ochoa, Eugenis de： Catálogo razonado de los manuscritos españoles existentes en la biblioteca real de Paris, Paris 1844

Ow，Josef：Die Abstammung der Griechen und die Irrtümer und Täuschungen des Dr Ph. Fallmerayer，慕尼黑，1847 年

Pallmann，Reinhold：Die Geschichte der Völkerwanderung von der Gotenbekehrung bis zum Tod Alarichs，哥达，1863 年

Paparregopulos, Constantine: ‛Ιστορία του̃ ‛Ελληνικου̃ έθνους απὸ τω̃ν αρχαιοτάτων χρόνων μέχρι τω̃ν νεωτέρων, 5 volumes.

康斯坦丁-帕帕雷戈普洛斯：《希腊文明史》，巴黎，1878 年

欧内斯特-帕里索 埃内斯特：《葡萄酒史》，第 2 卷，巴黎，1862 年及以后。

Peruzzi, S. L.: Storia del Commercio e dei Banchieri di Firenze, Florence 1868

彼得森、弗里德里希-克里斯蒂安：《考古学研究导论》，莱比锡，1829 年

Pittakis, K. L.: L'ancienne Athènes, ou la Description des antiquités d'Athènes et de ses environs, Athens 1835

Plancher, Urbain: Histoire générale et particulière de Bourgogne, 4 vols, Dijon 1739-1781

弗朗索瓦-普克维尔：《格拉西游记》，6 卷，巴黎，1826-1827 年

汉斯-普鲁茨：《十字军东征文化史》，柏林，1883 年

Rambaud, A.：L'Empire grec au Xe siècle，Paris 1870

Rangabe，Jakob R.：《Τὰ ‛Ελληνικά》，3 卷，雅典，1853 年

Riant，Paul：Expéditions et pélerinages des Scandinaves en Terre Sainte au temps des croisades，巴黎，1865 年

Riant, Paul: Exuviae sacrae Constantinopolitanae, 3 vols, Geneva 1877 ff.

Röhricht，R./Heinrich Meisner：德国圣地朝圣之旅，柏林，1880 年

Romanin, Samuele： Storia documentata di Venezia》，10 卷，威尼斯，1853-1861 年

罗尔斯、路德维希：《中世纪希腊历史文献》，慕尼黑，1837 年

Roß，Ludwig：Die Demen von Attika und ihre Verteilung unter die Phylen，M. H. E. Meier 编辑，哈雷 1846 年

路德维希-罗萨斯：《奥托国王和阿玛丽亚王后在希腊的旅行》，第 2 卷，哈勒，1848 年

路德维希-罗萨斯：《考古论文集》，2 卷，莱比锡，1855-1861 年

Rubió y Lluch，Antonio：La expeditionición y dominación de los Catalanos en Oriente jusgadas por los Griegos，巴塞罗那，1883 年

Rubió y Lluch, Antonio: Los Navarros en Grecia y el ducado Catalán de Atenas en la época de su invasión, Barcelona 1886

Rubió y Lluch，Antonio：Don Guillermo Ramón Moncada，gran senescal de Cataluña，巴塞罗那，1886 年

保罗-约瑟夫-萨法里克：《斯拉夫古迹》，海因里希-武特克编辑，第 2 卷，莱比锡，1843-1844 年

Saint-Genois de Grandbreucq, François: Droits primitifs des anciennes terres, 1st vol., Paris 1782

Sassenay, Fernand de： Les Brienne de Lecce et d' Athènes》，巴黎，1869 年

Sathas，Constantine：Τουρκοκρατουμένη ‛Ελλάς，1453-1821 年，雅典，1869 年

Saulcy, Félicien de： 克罗伊萨德钱币学》，巴黎，1874 年

Schaefer、Heinrich：《西班牙史》，哥达 1830 年（= F.W.Lembke、H.Schaefer、Fr.W.Schirrmacher：《欧洲国家史》，第 2-3 卷）

Schlumberger, Gustave：《东方拉丁钱币学》，巴黎，1878 年

Schlumberger, Gustave: Sigillographie de l'Empire byzantin, Paris 1884 年

Schmidt, Bernhard: Das Volksleben der Neugriechen und das Hellenische Altertum, Leipzig 1871

施密特-E.A.：《中世纪阿拉贡史》，莱比锡，1828 年

Schmitt, John：Die Chronik von Morea, Eine Untersuchung über das Verhältnis ihrer Handschriften und Versionen zueinander，慕尼黑，1889 年

F.W.西伯：《克里特岛之旅》，2 卷，莱比锡，1823 年

西弗斯、戈特洛布-莱因霍尔德：《从利巴尼乌斯的生平谈起》，汉堡，1863 年

西弗斯、戈特洛布-莱因霍尔德：《罗马皇帝史研究》，柏林，1870 年

雅各布-斯庞：《阿特讷市现状报告》，古代希腊首都，建于 3400 年，里昂，1674 年

斯庞、雅各布：《根据格拉西航行论文撰写的书信》，乔治-吉列-德-圣-乔治编，巴黎，1679 年

斯邦、雅各布/乔治-惠勒： Italienische, Dalmatische, Griechische und Orientalische Reise-Beschreibung，2 卷，纽伦堡，1681 年

Stamatiadis, Epaminondas： Οι Καταλανοί εν τη̃ 'Ανατολη̃', 雅典，1869 年

Streit, Ludwig: Venice and the turn of the fourth crusade against Constantinople, Anklam 1877

Summonte、Antonio Giovanni：《Istoria Napoli》，4 卷，那不勒斯，1675 年

Surmelis，Dionysios：Κατάστασις συνοπτικὴ τη̃ς πόλεως 'Αθηνω̃ν, 雅典，1842 年

Surmelis, Dionysios: Αττικὰ ὴ περὶ τω̃ν Δη̃μων 'Αττικη̃ς, 雅典，1855 年

Sybel, Ludwig von： 雅典雕塑目录》，马尔堡，1881 年

Tafel、Gottlieb L. F.：De Thessalonica，图宾根，1839 年

Tafel, Gottlieb L. F.: Symbolarum criticarum, geographiam Byzantinam spectantium, 2 vols.

Tafel, Gottlieb L. F./Georg Martin Thomas：Urkunden zur Handels- und Staatsgeschichte der Republik Venedig，3 卷，维也纳，1856-1857 年

Tanfani，Leopoldo：Niccola Acciainoli，studi storici，佛罗伦萨，1803 年

Targioni-Tozzetti，Giovanni：Relazioni d'alcuni viaggi fatti in diverse parti della Toscana，12 卷，佛罗伦萨，1768-1779 年

Testa, F.: De vita et rebus gestis Frederici II. Siciliae regis，巴勒莫，1775 年

Thomas, Georg Martin：Diplomatarium Veneto-Levantinum 1300-1350，威尼斯，1880 年

Tournefort, J. de： Relation d'un voyage au Levant, Paris 1717

乌尔曼（C. Ullmann）：《神学家拿齐安祖的格雷戈里乌斯》，哥达，1867 年

乌尔里希斯、海因里希-尼古拉斯：《希腊旅行与研究》，第 2 卷，柏林，1840-1863 年

Unger，Friedrich Wilhelm：Christlich-griechische oder byzantinische Kunst，载于 Ersch/Gruber (ed.)：Allgemeine Encyclopädie der Wissenschaften und der Künste，第 84 卷，第 291-474 页，第 85 卷，第 1-66 页，莱比锡，1866 年，1867 年

Voigt, Georg: Enea Silvio de' Piccolomini, as Pope Pius the Second and his Age, 3 vols.

Wachsmuth, Kurt: Die Stadt Athen im Altertum, vol. 1, Leipzig 1874 (vol. 2, Leipzig 1890)

韦伯、卡尔-弗里德里希：De academia literaria Atheniensium seculo secundo post Chr.

Wietersheim, Eduard von： Geschichte der Völkerwanderung，4 卷，莱比锡，1859-1864 年

弗里德里希-威尔肯：《十字军东征史》，3 卷，莱比锡，1807-1817 年

扎卡里亚-冯-林根塔尔、卡尔-爱德华：《希腊罗马法》，7 卷，莱比锡，1856-1884 年

Zachariä von Lingenthal，Karl Eduard：Geschichte des Griechisch-Römischen Privat-Rechtes，莱比锡，1864 年（第二版，标题为：Geschichte des griechisch-Römischen Rechts，柏林，1877 年，1892 年）。

Zanelli，Agostino：Le schiave orientali a Firenze nei secoli XIV e XV，佛罗伦萨，1885 年

Zeller，Eduard：Die Philosophie der Griechen in ihrer geschichtlichen Entwicklung，6 卷，1862-1882 年

宙斯、约翰-卡斯帕：《日耳曼人和邻近部落》，慕尼黑，1837 年

津凯森、约翰-威廉：《奥斯曼帝国在欧洲的历史》，7 卷，汉堡，1840-1863 年

祖姆普特、卡尔-戈特洛布：《论雅典哲学学校的存在》，柏林，1843 年
