#### 2.

压抑已久的民族政党在雅典蠢蠢欲动。在法兰克统治下的雅典历史上第一次出现了这样的政党。当地的大主教家族再次崛起，加入了希腊大主教的行列。大主教马卡里奥斯一定是被雅典捐赠给拉丁教会的举动激怒了；民族仇恨蒙蔽了他的双眼，他秘密地与土耳其人谈判，在尼里乌斯死后几个月，塞萨利的帕夏-蒂穆尔塔什率领一支军队入侵了阿提卡。他毫无抵抗地占领了雅典下城。只有卫城（西班牙人在位时加固了卫城）被勇敢的法警马特奥-德-蒙托纳（Matteo de Montona）占领，他是尼里欧斯遗嘱的执行人之一，在威尼斯的文件中，这个名字被拼写成 Montona，而不是 Mentona。在伊斯特拉有一个叫蒙托纳的堡垒，马特奥可能就来自那里。

他在窘境中派信使前往内格罗蓬特，请求那里的威尼斯法警安德烈亚-本博（Andrea Bembo）通过营救的方式解放他，并像雅典城一样，在保证雅典人的自由和权利的条件下，为共和国占有这座城堡。贝姆博批准了这一请求，但须经总督确认。他从欧博亚派兵迫使土耳其人离开雅典和阿提卡。随后，蒙托纳向威尼斯人开放了雅典卫城，1394 年底，塞克罗普城堡的城垛上第一次升起了圣马可的狮子旗帜。(Muratori XXIII，第 1075 页）。由于威尼斯参议院 1395 年 3 月 18 日的决议指出蒙托纳的信使已经在威尼斯逗留数月，因此拜洛人占领雅典不可能像霍普夫假设的那样发生在 1395 年初。

安德烈亚-本博（Andrea Bembo）向总督报告了这一重要事件，而马蒂奥-德-蒙托纳（Matteo de Montona）则派博洛尼亚的莱昂纳多（Leonardo of Bologna）作为自己的全权代表前往总督处，要求共和国承认雅典已被占领的事实，并确认白罗的合同承诺。威权统治者只能满意地欢迎其在黎凡特的第一任大臣的大胆行为，尽管这样做的后果一定会引起许多疑虑；因为获取雅典和阿提卡必然会遭到共和国所有敌人的激烈反对，包括苏丹、伯罗奔尼撒半岛的拜占庭人和尼里欧的继承人。只有威尼斯有无可争议的权利和义务保护和拯救雅典。1395 年 3 月 18 日，元老院通过了一项决议，主张占有该城："Intromissio Athenarum"（Archiv Venedig, Deliber. Miste del Senato I, vol. 43, fol. 50）"Intromissio "与 "acceptatio "一样，"intromittere "的否定是 "dimittere"。我在 Sitzungsberichte der K. Bayer. Akad.

在这一行动中，他宣布不允许放弃雅典，否则雅典就会落入土耳其人之手，共和国所珍视的邻近领土就会像眼睛的瞳孔一样遭到毁灭。威尼斯接管雅典城时，明确承认其所有的权利、自由、特权和传统习俗，拜洛-内格罗蓬特在与马特奥-德-蒙托纳签订的条约中已经向他宣誓维护这些权利、自由、特权和传统习俗。作为对这位 "将雅典拱手让给威尼斯的主要功臣 "的英勇督军所做贡献的奖赏，他每年可从城市收入中获得 400 金圆券的抚恤金；博洛尼亚的莱昂纳多和另外两名雅典人贾科波-哥伦比诺（Giacopo Columbino）以及公证人马克里（Makri，希腊人，也曾为威尼斯人做出过努力）获得的金额较少。

共和国将雅典事务的重组保留到充分了解雅典收入数额的时候。议会的决定明确提到了尼里欧的遗嘱，根据该遗嘱，共和国将接管雅典的统治权："Quod dominium dicte civitatis Athenarum recipiatur er asumatur gubernandum et regendum per dominationem nostram secundum formam testamenti D. Nerii de Azaiolis"。小镇向教堂的捐赠在沉默中进行。已故公爵的马厩是圣玛丽教堂的主要收入来源，但由于马匹被盗，马厩的收入减少了，而且保护雅典的费用也增加了，因此决定暂时将教士人数减少到八人。未来的威尼斯校长将与圣玛丽教堂的两名授权代表或检察官一起管理教会的收入和维持。

威尼斯共和国攻占雅典在希腊邻国引起了轩然大波。事情一发生，卡洛-托科就对阿尔戈斯甚至阿提卡进行了敌对性的远征，并派出使节回应威尼斯的控诉。1396 年 5 月 26 日，他在 "castro S. Georgii de insula mea Cephalonie "写给总督的信中称自己为 "vr. fidelis civis filius et servitor Karolus Dux Lucate et Comes Cephalonie palatinus"（Commem.IX，fol.14）。人们还担心与土耳其人结盟的希腊民族党方面会发生动乱。因此，拜洛-内格罗蓬特确保自己掌握了后者的领袖；他逮捕了马卡里奥斯，并将其送往威尼斯。大主教一直被囚禁在那里；由于他还被指控与土耳其人谈判，教皇博尼法斯九世于 1396 年 5 月 27 日命令吉尔伯特主教对他进行审判。contra Macaronum archiep. Athenar. Dat. Kal. Junii P. N. a. VII" (Commem. IX, fol. 15)。

雅典政府将共和国交给了一位拥有执法官和督军头衔的贵族，就像瑙普利亚和阿尔戈斯由这样一个人管理一样。"Scribatur potestati et capitaneo Athenarum"（一个人），威尼斯的法令中是这么说的（Sathas, Mon. Hist. Hell. II, n. 212），或者干脆就是 "potestas. Ser. Nicolaus Victuri iterum potestas Athenarum"（1400 年 8 月 3 日，同上，第 222 页）。然而，当时的雅典和低廉的薪水对高傲的贵族们毫无吸引力，以至于总督安东尼奥-维尼耶起初遭到了许多人的拒绝。第一个以雅典执法官头衔自居的威尼斯人是阿尔巴诺-孔塔里尼。他于 1395 年 7 月 27 日被任命为雅典波德斯塔总督，任期两年，薪水为 70 英镑，其中包括一名公证人、一名威尼斯人助理（socius）、四名仆人、两名农夫和四匹马。与此同时，卫城还任命了两名弓箭手队长，月薪为 6 杜卡特；其中一人白天必须在要塞，但两人晚上都必须在要塞。"Duo capta ballistariorum"；又称 "castellani"。1400 年 4 月 20 日，共和国命令雅典执法官替换被解职的约安尼斯-瓦拉乔（Joannes Valacho），"unius ex castellanis... alium castellanum sive caput"（Sathas II, n. 212）。由于当时城堡的守备部队规模极小，共和国认为康塔里尼用 20 名士兵加强卫城的守备部队就足够了。如果需要更多的战士和资金来保护城市，执法官奉命向莫东和科龙的卡斯特罗或内格罗蓬特斯的贝洛寻求支持。由于之前有 1395 年 7 月 27 日的法令，该委员会可能是同一天成立的。7 月 18 日，康塔里尼已经被任命，因为元老院下令，如果即将赴任的康塔里尼没有发现内格罗蓬特的桨帆船配备齐全，他将被坎迪亚或群岛的桨帆船引领到雅典（Misti XLIII， cart. 71）。

1395 年夏天，孔塔里尼抵达雅典，住进了雅典卫城上的阿卡约利宫殿。这座城市的权利和市政宪法都没有改变，可能很快就感受到了威尼斯人政府的好处，但它和阿提卡已经陷入了贫困，以至于在第二年，孔塔里尼向共和国申请了 3000 杜卡特的贷款，共和国批准了为期两年的贷款。 "Considerata paupertate dicte terrae, ut non perveniat ad extremitatem (Misti XLIII, cart. 155, 6 Oct. 1396, Ind. V)。

雅典公国现在可以被视为已经消亡了；科林斯只是被尼里欧联系在一起，属于米西特拉的暴君西奥多，梅加里属于托科，波提亚属于安东尼奥-阿卡约利；只有阿提卡，就像阿戈利斯的地貌一样，处于威尼斯的控制之下。但在此期间，雅典公爵以前的行省或男爵领地也被土耳其入侵的浪潮所席卷。

1393 年，巴雅西德征服了维丁、尼科波利斯和西里斯特里亚，除掉了保加利亚最后一位国王西斯曼，并将其土地并入自己的帝国。随后，他占领了黑海沿岸仍属于皇帝的城市和马其顿的沿海土地，同时在君士坦丁堡城墙外驻扎军队，将君士坦丁堡与外界隔绝开来。在塞雷斯，他召集了向他纳贡的希腊王公，包括暴君西奥多，曼努埃尔的弟弟。他厌倦了他们无力的阴谋诡计，决定最终征服希腊各省。他派兵渡过奥特里斯河，攻占了拉里萨、法萨拉和泽屯，下到斯佩切欧斯山谷，占领了以前属于雅典公国的尼奥帕特拉，然后突破了没有设防的特莫比莱河，进入了福西斯和洛克里斯。

末代法德里克的遗孀海伦娜-坎塔库泽纳当时仍在统治萨洛纳，或者说她的情人--一名司铎--在那里充当着令人憎恨的暴君。一些希腊人与土耳其人结盟；据说法西斯的大主教塞拉芬是他国家的叛徒，他把苏丹召到了这些美丽的狩猎场。土耳其人一出现，海伦娜就向他们打开了城门。她被巴哈西德体面地看管起来，而她的女儿玛丽亚则流落到了巴哈西德的后宫。萨洛纳伯爵家族就此灭亡；这座城市和弗西斯的风景都变成了土耳其的。Chalkokond. (lib. II, p. 67ff.) 中的一些段落有误；他明确指出唐-路易斯是苏拉的领主和海伦娜的亡夫。Galaxidi 编年史》中关于最后一位萨洛纳伯爵的结局的记载相当混乱，但始终表现出一个可怕的悲剧，其女主角是一个邪恶的女人。即使在今天，在古老的洛克里安菲萨，卫城坚固的塔楼和一座法兰克教堂也会让我们想起斯特罗蒙库尔和加泰罗尼亚人的时代。
