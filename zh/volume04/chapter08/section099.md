#### 3.

只有真正的观察才能消除 "雅典已毁 "这一顽固不化的观点，而这正是法国耶稣会士和卡普钦会士的功劳。1645年，雅典接待了法国耶稣会士和卡普钦会士；当他们前往内格罗蓬特时，其他人取代了他们的位置。1658 年，卡普钦派从土耳其人手中买下了莱西克拉特斯的纪念碑，即所谓的德摩斯梯尼灯塔，并在旁边修建了修道院。这座优美的古雅典人戏剧崇拜纪念碑成为研究雅典地形和古迹的起点。法国僧侣们绘制了雅典城的第一批平面图。通过这种方式，法国人为科学提供了他们的祖先在勃艮第公爵时代欠下的服务。法国使节甚至从君士坦丁堡访问雅典。如果说路易十三的驻雅典大使路易-德-海耶斯（Louis de Hayes）在1630年的访问中只是对这座奇妙的城市投去了好奇的一瞥，那么1674年至1675年冬季诺因泰尔侯爵的逗留则产生了重要的影响。雅克-卡雷（Jacques Carrey）为他绘制了帕台农神庙的雕塑，意大利人科内利奥-马格尼（Cornelio Magni）写了一篇游记。1674 年，在 Nointel 来访之前，博学的医生 Spon 在里昂发表了耶稣会士 Babin 撰写的雅典记述，Babin 曾在雅典居住过很长时间，他为侯爵在君士坦丁堡的管家 Abbé Pécoil 撰写了这篇记述。他于 1672 年 10 月 8 日从士麦那发来的报告为对这座城市进行更详细的考察提供了主要动力。吉列的《新旧雅典》一书于 1675 年出版。这位法国人从未去过雅典，但他使用了巴宾的信，并从卡普钦人那里获得了其他信息和城市地图，这使他的著作极具价值。Avec le plan de la ville d'Athènes》，第 3 版，巴黎，1676 年，第 12 册。 随后，斯庞本人也出现了。在英国人乔治-惠勒爵士（Sir George Wheler）的陪同下，他于 1676 年 1 月抵达雅典，他的研究创立了现代雅典古代科学。

值得注意的是，当时的旅行者在雅典没有再发现弗兰肯人。科尔内利奥-马格尼只见过法国领事夏泰尼耶（Chataignier）和英国领事吉罗（Giraud），他们都是受过教育的人，是所有好奇游客的向导。在城市一级家族中，他挑出了 Chalcocondyli、Palaiologi、Beninzeloi、Limbona、Preuloi 和 Cavalaroi 家族，其中一些家族源于拉丁语。在 Nointel 时代，雅典有一位德国冒险家，来自西普鲁士的斯特拉斯堡，名叫 Georg Transfeldt。他的拉丁文残篇。阿道夫-米凯利斯（Adolf Michaelis）出版了他的拉丁文自传片段：Examen reliquarum antiquitatum Atheniensium (Mitteil. d. D. Arch. Inst. in Athens, 1876)。贝宁泽洛家族似乎是意大利后裔。约翰内斯（Johannes）是 18 世纪一位博学的雅典人。Chalkokondyli 属于历史上著名的家族，但他们的名字在人们的发音中已改为 Charkondyli。在哈德良体育馆的长廊（Taxiarchs 教堂所在地），16 世纪的涂鸦碑文提到了一个叫路易兹（Louize）的人和一个叫迈克尔-查孔迪里（Michael Charkondyle）的人。雅典的许多洗礼名字都是从法兰克时期流传下来的，如 Guliermos、Phinterikos、Benardes、Linardis、Nerutzos (Nerio)。Zesios in Deltion der histor. 这座教堂似乎是由 Michael Chalkokond 建造的。即使在土耳其时期，虔诚的雅典人仍在建造教堂。Chalkokondyloi 家族至今仍居住在那里。

同样是在 17 世纪的后三分之一，当法国和英国的博学旅行家向西方人介绍雅典仍然存在的古代遗迹时，这座古老的城市突然被土耳其人用武力夺走了。长期以来令欧洲闻风丧胆的苏丹帝国开始衰落。古兰经》的基本律法规定，只有两类人，即穆罕默德教徒和不信教者，就像古希腊人只有希腊人和野蛮人，犹太人只有耶和华的信徒和异教徒一样，这一与现代世界条件格格不入的教条宣判了奥斯曼奴隶制国家的死刑。它使自己陷入了永恒的野蛮状态。它无法超越被征服民族的原始压迫状态。土耳其的统治是通过征服世界上最伟大的国家组成的，但它却无法建立一个像亚历山大帝国和拜占庭帝国那样的文化帝国。这个亚洲国家在欧洲仍然是一个充满敌意的反常现象，它与欧洲的体系格格不入，土耳其出于迟钝和宗教狂热，没有参与欧洲的经济和思想发展。只要奥斯曼人的征服冲动继续存在，只要西方人的政治本性助长了奥斯曼人的征服冲动，只要奥尔汗、穆罕默德和苏莱曼的军事机构支持奥斯曼人的征服冲动，奥斯曼人就会感到可怕。归根结底，君士坦丁堡的归属已成为一个无法解决的问题，它为基督教列强的恐惧和忌妒提供了一个不可估量的重要目标，这才是君士坦丁堡得以继续存在的唯一原因。因此，他们仍在焦虑地努力推迟问题的解决，甚至在我们这个时代，名义上的亚历山大（他的胜利大军驻扎在拜占庭大门口的圣斯特凡诺）也无法鼓起勇气，像大胆的丹多罗总督曾经敢于并做到的那样，用剑切开现代政治的这个戈尔迪之结。

三十年战争及其影响使苏丹们最后的征服成为可能。瓦伦斯坦瓜分欧洲土耳其的大胆计划未能实现。1669 年，基督教列强允许大维齐尔艾哈迈德-科普里利从威尼斯人手中夺取了克里特岛。直到 1683 年，维也纳的洗劫才带来了一个转折点：土耳其的势力重新回到了南方。苏丹不仅不得不放弃匈牙利，还失去了莫雷亚。被排除在地中海之外的威尼斯共和国与列强联盟联手，孤注一掷，试图重新夺回其在黎凡特的地位。自君士坦丁堡和雅典陷落以来，希腊的解放从未像威尼斯自1685年以来发动的土耳其战争那样如此接近实现，希腊的解放是欧洲哲学家们不懈的柏拉图梦想，也是被奴役的希腊人的渴望。

帕特雷战役胜利后，雅典使者召集弗朗切斯科-莫罗西尼总指挥解放他们的故乡。1687 年 9 月 21 日，共和国舰队驶入比雷埃夫斯。威尼斯人第三次占领雅典。马克国王围攻雅典卫城。26 日，一枚不幸投掷的炸弹炸碎了帕台农神庙的一半，这座神庙迄今已历经两千多年的风雨。城堡里的土耳其守军投降了，他们和城里的 2500 名穆斯林居民一起前往小亚细亚。然而，雅典人对自由的渴望只持续到 1688 年 4 月 9 日，刚刚被任命为总督的莫罗西尼放弃了难以维持的雅典。他想把帕台农神庙西侧山顶的圆柱作为战利品带走，但他的行动失败了；尼普顿像、胜利女神的战车连同战马和其他大理石雕塑都掉了下来，摔得粉碎。莫罗西尼只设法安然无恙地带走了仍矗立在威尼斯兵工厂门前的雅典狮子，它们是希腊解放和雅典艺术品被掠夺的失败纪念碑，是S.A.门上方青铜骏马的陪衬。雅典人被剥夺了希望，为了躲避乘威尼斯船只返回的土耳其人的愤怒，他们在萨拉米斯、埃吉纳和共和国征服的基克拉迪群岛找到了避难所。这座城市一直荒废了三年，直到 1690 年，由于拜占庭宗主教的干预，苏丹才大赦雅典人，允许他们返回被烧得半毁的故乡。流亡的雅典人向宗主教求情，此前他们因不明原因被宗主教逐出教会： Surmelis, Katastasis, p. 71 ff. 众所周知，法尔梅拉耶受所谓的阿纳吉里修道院碎片的诱惑，将雅典被遗弃的三年变成了四百年，并将其转移到六世纪至十世纪。

1699 年 1 月 26 日签订的《卡尔洛维茨和约》确保了威尼斯共和国对莫雷亚的占有，但它只能在短时间内守住半岛。土耳其帝国虽然内部支离破碎，但仍有战争能力，它从 18 世纪初欧洲的动荡中获得了新的喘息机会，甚至新的利益。彼得大帝的征服者艾哈迈德三世于 1715 年从威尼斯人手中夺回了莫雷亚，虽然尤金王子的胜利迫使他在奥地利面前蒙受了巨大损失，但他还是在 1718 年 7 月 21 日的《帕萨罗维茨和约》中保住了伯罗奔尼撒半岛。和约条款给予希腊人人身自由。

威尼斯人对雅典转瞬即逝的占有给这座城市的古迹造成了无法弥补的损失，也给人民带来了新的苦难。只有科学因莫罗西尼的军事行动而受益匪浅。威尼斯工程师弗内达和圣费利斯为雅典卫城和当时的城市绘制了更精确的平面图；法内利将其发表在《雅典卫城》一书中。在这本书中，他还用几个段落介绍了法兰克公爵时期的情况。Atene Attica descritta da suoi principii sino all' acquisto fatto dall' armi venete nel 1687 con varietà di medaglie, ritratti et disegni, Venezia 1707, 4.

杜康（Du Cange）是我们了解拜占庭中世纪的不朽奠基人，他在 1657 年出版的《法国皇帝统治下的君士坦丁堡帝国史》中已经阐明了希腊在法兰克人统治时期的命运。奇怪的是，法国人随后将这些研究领域，尤其是雅典研究，在一段时间内留给了其他国家，首先是英国。自白金汉和阿伦德尔时代起，英国人就热衷于收藏希腊文物；这种热情仍然是 19 世纪初掠夺埃尔金的罪魁祸首。富有的领主们或派遣代理人前往希腊和东方，或亲自前往希腊和东方，比如克莱蒙特勋爵，理查德-道尔顿在 1749 年为他绘制了雅典纪念碑和雕塑的图纸。

自 1751 年以来，优秀的艺术家、斯图亚特家族和雷维特家族一直在探索这座城市，他们的劳动成果就是他们的巨著《雅典古迹》。随后，1734 年在伦敦成立的迪莱特协会（Society of Dilettantes）也发起了其他调查，1776 年钱德勒的《希腊游记》记录了这些调查。英国人的研究热情一直持续到 19 世纪。希腊对拜伦勋爵的天才充满感激之情，不会很快忘记马丁-利克和乔治-芬利等人的功绩。

因此，科学的力量重新唤醒了人类对雅典的热爱。科学的力量让所有愿意接受理想的人看到了这座城市昔日的辉煌，而世界正是在这座城市中接受了最优秀的教育；科学的力量还在所有文明国家掀起了希腊主义的第二次复兴，它就像黎明前的曙光一样照亮了希腊的真正解放。
