#### 2.

总的来说，土耳其统治时期雅典和希腊的历史学家有一项艰巨而令人不快的任务要完成；除了苏尔梅里斯外，我首先要提到康斯坦丁-萨塔斯（Constantine Sathas，Τουρκοκρατουμένη ‛Ελλάς，1453-1821 年）（1869 年，雅典）和苏格兰人芬利。德梅特里奥斯-坎普罗格卢（Demetrios Kampuroglu）刚刚开始出版土耳其统治下雅典人的历史，该书有望带来许多新内容。他看到眼前是一片荒漠，他在其中寻找生命的身影和迹象，他的眼睛还能抱有希望。他的目标始终指向这块高贵土地的最终解放，他倾听克莱夫特和帕利卡伦的每一首歌，让自己相信希腊的缪斯女神仍在她的石棺中激荡，自由的思想仍让堕落的希腊人心潮澎湃。雅典城本身就像一个被卖到野蛮世界的奴隶，她已经消失或失踪了。她昏昏欲睡，对苦难和卑贱习以为常，偶尔被希腊海域的战争喧闹声惊醒，她的处境如此无望，以至于她预料到她的奴隶主的敌人的逼近会加重她的命运，而不是拯救她。

与土耳其人的战斗不得不由陆地上的匈牙利、波兰和奥地利以及海上的威尼斯接手。在苏丹进入拜占庭的那一刻，这个辉煌的共和国衰落了，它的衰落很快就被世界上的其他状况所取代。它的强大在很大程度上归功于希腊、殖民地和东海贸易。它的财富和权力来源于此。与 13 世纪以来的崛起相比，它的衰落或许更显英勇。它将土耳其人从亚得里亚海击退，并保卫了黎凡特的先驱--爱奥尼亚群岛。三个世纪以来，圣马可之狮保护欧洲免受亚洲野蛮人的入侵。

威尼斯人多次奋起反抗苏丹。1464 年 7 月，他们甚至在其总指挥维托雷-卡佩罗（Vettore Capello）的率领下入侵雅典，此前他们曾在阿卡约利时期统治过雅典。不幸的是，他们的雇佣兵无情地虐待了这座城市，使威尼斯的名声蒙羞，他们袭击、掠夺并迅速放弃了这座城市，却不敢尝试攻打防御严密的雅典卫城。同年，斯巴达也被威尼斯人占领了几天。他们的管家西斯蒙多-马拉泰斯塔家族将 1450 年左右死在那里的普列通的遗体运到里米尼，并安葬在著名的大教堂里。

威尼斯和热那亚的希腊殖民地相继落入土耳其的控制之下。尤博亚岛在威尼斯人的英勇抵抗下沦陷；1470 年 7 月 12 日，穆罕默德二世进入了冒烟的内格罗蓬特废墟。威尼斯因此失去了其在爱琴海长期守护的领土。30 年后，土耳其人还征服了莫罗敦和科伦的莫罗敦殖民地，然后是埃伊纳，1522 年征服了属于马耳他骑士团的罗得岛，1540 年征服了罗马尼亚的那不勒斯和蒙巴萨。

奥斯曼帝国在苏莱曼一世（1519-1560 年）的统治下达到了权力的顶峰，这位统治者的天才和实力在查理五世的伟大时代无人能及。它还延伸到了多瑙河以外。由于苏丹夺取了半个匈牙利，并已威胁到维也纳，亚欧之间的战场从巴尔干半岛转移到了多瑙河中游地区。正是在这个时候，最重要的任务和内部危机困扰着西方。教育的复兴、过时的教会改革、查理五世君主制的出现、一个新的世界帝国适时地与奥斯曼帝国对抗、西班牙-奥地利与法国之间争夺欧洲霸权的斗争，所有这些事件都吸引着西方的参与，使他们不再关注希腊的命运。

在先知的旗帜插上君士坦丁堡的圣索菲亚大教堂和雅典的帕台农神庙之后，基督教的十字架在格拉纳达的阿尔罕布拉宫升起。摩尔人在西班牙的统治被摧毁后，欧洲在东方因穆罕默德人而遭受的损失即使不能弥补，至少也减少到了最低程度。美洲的发现和殖民为人类在大洋彼岸的新世界和新未来开辟了不可估量的前景，而通往印度的海路的发现则使古老的地中海东方贸易路线失去了独有的意义：这不仅是威尼斯衰落的原因之一，也是奥斯曼帝国衰落的原因之一。在地球的整个历史上，没有任何一个时刻像当时一样，如此众多、如此巨大的生命之流汇聚在一起，使各国人民充满了新的精神，使他们从王朝利益政治和平庸的资产阶级堆砌的狭隘范围内上升到整个世界的意识。哥白尼摧毁了希腊人过时的地球静止体系。

在15世纪，欧洲没有抓住时机奋起反抗土耳其帝国的巨无霸，仅凭最卑鄙的政治本能统治着欧洲，并冷静地抛弃了希腊，这一点可以受到批评，但到了16世纪，西方接受了这一既成事实，视希腊为失落，几乎将其遗忘，这是可以理解的。雅典从欧洲舞台上消失了。早在 1493 年，一位德国人文主义者就在他的编年史中写道："雅典城是阿提卡地区最辉煌的城市；它的一些痕迹依然存在"。Cujus pauca vestigia quedam manent"。哈特曼-斯切尔（Hartmann Schedel）从埃涅阿斯-西尔维乌斯（Aeneas Sylvius）的欧罗巴（Europa）中摘取了这句话和其他一些文字。我们已经看到，Schedel 是以德意志城市的形象来描绘雅典的。

拉博德收集了 16 世纪西方关于雅典命运的零星记载，并指出这些记载是多么贫乏。1537 年，Jehan de Vega 随法国船队来到黎凡特，他在游记中写道，他在雅典的港口 Porto Leone 看到了一只大石狮子，但他没有进入雅典城。在苏尼翁角，领航员告诉他，在亚里士多德教授哲学的神庙圆柱上方有一座建筑，就像雅典的阿雷奥帕克斯市政厅仍然建在圆柱上一样。威廉-波斯特尔（Wilhelm Postel）在 1537 年至 1549 年间游历了希腊、君士坦丁堡和小亚细亚，并撰写了一部关于雅典共和国的博学著作，但他似乎认为不值得费力去雅典。法国人安德烈-特韦（Andrée Thevet）是《黎凡特宇宙学》的作者，他声称自己在 1550 年去过雅典，但他对雅典的描述除了这些调侃之外别无其他： 在一个叛徒的家里，他看到了一尊美丽的大理石雕像，但这座城市没有其他值得注意的地方。"当然，那里确实有一些圆柱和方尖碑，但它们都已成为废墟；还有一些学院的痕迹，居民们普遍认为柏拉图曾在那里讲课。它们具有罗马斗兽场的形状。现在，这座昔日如此闻名的城市已被土耳其人、希腊人和犹太人居住，他们对这些奇特的古物并不尊重。雅典的犹太人是 Thevet 虚构出来的。

1571 年 10 月 7 日，西班牙、奥地利和罗马三国在唐-胡安-德奥的率领下，在莱潘托取得了海军大胜。通过文艺复兴所编织的古典科学之线，雅典本身与受过教育的欧洲联系在了一起。学术界需要对这座辉煌城市的命运有一定的了解，这体现在它是否还存在的问题上。德国哲学家、图宾根古典文学教授马丁-克劳斯提出了这个问题。这个问题使他永垂不朽，正如拉奥孔群的发现使一位无名罗马人名垂青史一样。马丁-克鲁修斯重新发现了雅典。

1573 年，他写信给君士坦丁堡的宗主教议长狄奥多西奥斯-齐戈马拉斯，想知道德国历史学家所说的科学之母已不复存在，除了几间渔舍外已从地球上消失的说法是否属实。这位受过教育的拜占庭人的回信，以及后来同一教区的神职人员、阿卡南人 Symeon Kabasylas 的来信，为这位德国学者带来了关于这座城市继续存在的第一个确切消息，也是他们第一次对这座城市的古迹和幸存者的状况有了微弱的了解。克鲁修斯将这些信件发表在他著名的 "Turcograecia "一书的第七卷第 10 页和第 18 页。

因此，在欧洲最光明的启蒙运动和最辉煌的艺术创作时期，雅典城的存亡确实比拜占庭时期更不为人所知，也更令人怀疑，甚至在 1835 年，一位德国学者还认为，在查士丁尼之后的四个世纪里，雅典城一直是一片无人居住的森林荒野。最初是科学为受过教育的西方人重新征服了雅典，而在 16 世纪，土耳其战争和前往封闭的阿提卡的不确定性使得这种征服变得非常困难。

与对罗马城的探索相比，对雅典古代的研究推迟了几个世纪。这是因为，从中世纪的罗马奇迹到真正描述罗马城的第一次重大进展是在 15 世纪由 Blondus Flavius 取得的，随后罗马古代研究得到了有力的发展。雅典人自己不可能对自己的城市做这样的事情。克里特岛和科孚岛的希腊人，聚集在威尼斯阿尔都斯-马努提乌斯（Aldus Manutius）周围的其他博学的希腊学家，活跃在曼图亚、帕多瓦、罗马、巴黎、日内瓦、海德堡等地希腊语文法学校的希腊人，都将精力投入到语言学批评中。

从 17 世纪初开始，荷兰人让-德-默尔斯（Jean de Meurs）通过他的大量研究为雅典古代研究奠定了基础，他的研究以勤奋和惊人的博学而著称。这本十二开本的文集还包括一部清晰介绍雅典历史的作品，直到雅典被土耳其人征服："Fortuna Attica, sive de Athenar. Batav. 1622.

罗斯托克人劳伦伯格的拉丁文著作《Genaue und sorgsame Beschreibung des alten und neuen Griechenlands》表明，当时人们对西方城市的状况知之甚少。在这里，作者重复了古老的寓言故事，声称："雅典曾被称为缪斯女神的居所，如今除了几间凄惨的小屋和小村庄外，什么也没有留下，它现在被称为塞廷".Description exacte et curieuse de l'ancienne et nouvelle Grèce composée en Latin par J. Lauremberg et traduite en Français, Amsterd. 不过，Lauremberg 知道 Crusius 的 "Turcograecia"，因为他在阿提卡历史概述中提到了 Kabasylas 的信。在他的阿提卡地图上，他将雅典描绘成一个圆形城市的缩影，城市中心耸立着一座高高的锥形山，山顶有两座哥特式尖塔。
