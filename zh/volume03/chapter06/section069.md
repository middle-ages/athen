### 第六章

阿卡约利家族 科林斯城主内里奥。色雷斯的土耳其人 罗杰-德-劳里亚在底比斯接待他们。公司的不利条件。蒙卡达（Matteo Moncada），副主教。彼得-德-普伊格的暴政。罗杰-德-劳里亚的管理。阿戈利斯的恩吉恩人。马蒂奥-德-佩拉尔塔，副主教。欧洲列强、教皇和土耳其人。底比斯大会。内里奥-阿奇亚约里征服梅加拉。路易斯-法德里克，副主教。法德里克家族。塔兰托的菲利普死后，Baux 家族继承了对亚该亚的所有权。

#### 1.

尼科洛-阿奇亚约里的遗嘱于 1358 年 9 月 30 日在那不勒斯起草，这份文件的范围之广、风格之独特不亚于一份王室文件，它将他的所有财产分配给了他的众多继承人。在他的儿子安杰洛、贝内德托和洛伦佐中，长子获得了梅尔菲郡、马耳他郡和意大利南部的其他领地，以及科林斯城堡和亚该亚的大部分土地。根据那不勒斯国王路易和王后乔安娜的许可（1354 年 9 月 8 日法案，Buchon, N. R. I, 第 83 页），安杰洛还获得了西西里大元帅的头衔。 在见证人中包括布里埃纳的沃尔特，他是名义上的雅典公爵，但他并不知道阿卡约利家族有一天会赢得他父亲的遗产。

顺便提一下，这位伟大的新贵的后代仍留在那不勒斯，但很快就年久失修了。然而，一个偶然的机会，阿奇阿霍利家族的一个分支在希腊再次兴盛起来。这个家族的首领是大元帅的表弟贾科莫，他与佛罗伦萨人巴托洛梅亚-里卡索利结婚后生下了三个儿子，分别是多纳托（Donato）、内里奥（Nerio）和乔瓦尼（Giovanni），他们都在希腊获得了财富。在尼科洛去世前不久，他任命多纳托为他在亚该亚土地的副手和科林斯的城堡主。通过他的影响，他的弟弟乔瓦尼在 1360 年成为了帕特雷的都主教，帕特雷是莫雷亚最大的大主教区，已经发展成为教皇管辖下的一个独立的精神公国。贾科莫的第三个儿子内利奥-阿奇亚约里早在 1363 年就带着大胆的计划出现在希腊；纳克索斯公爵乔瓦尼一世-萨努多死后，他向其梦寐以求的女继承人菲奥伦扎求婚，但威尼斯阻止了这一结合。

1364 年 9 月 16 日，名义上的塔兰托皇帝罗伯特在没有继承人的情况下去世，年轻的内里奥陪伴着他的遗孀波旁王朝的玛丽皇后，与她与塞浦路斯国王彼得一世的弟弟、卢西尼昂的盖伊的第一次婚姻所生的儿子加利利的休一起，试图为他赢得莫雷亚。与大元老一样，内里奥也因拜占庭一位名义上的女皇的宠爱而发迹。他从她那里买下了沃斯提察（旧 Aegion）和尼韦莱（夏尔皮尼家族的前男爵领地）。 然后，他自封为科林斯的领主。

大元帅的长子安杰洛被君士坦丁堡的新任名义皇帝，也就是罗伯特的兄弟安茹-塔伦图姆的菲利普二世确认拥有该领地；1366 年 11 月 7 日在那不勒斯的法令（Buchon, N. R. II, p. 204 ff., n. XXXIII）。1371 年 2 月 26 日，根据布林迪西的文凭，菲利普二世任命安杰洛-阿奇亚约利（Angelo Acciajoli）为科林斯的行宫伯爵；同上，第 208 页，第 XXXV 节。由于科林斯的省督多纳托（Donato）被召回意大利，安杰洛派他的弟弟内利奥（Nerio）前往科林斯担任行宫伯爵，并将这座城市连同西京（Sikyon）或巴西利卡（basilica）作为抵押借给了他。尼里欧-阿奇亚约里由此开始出现在莫雷亚，他在那里建立了自己的统治，而此时的希腊正变得越来越混乱。

当地统治者之间无休止的争斗，以及皇帝坎塔库泽诺斯和他的守誓人约翰五世之间再次爆发的内战，为奥斯曼人进入欧洲铺平了道路。据传说，1354 年，奥尔汗大胆的儿子苏莱曼在一夜之间渡过了赫勒斯滂海峡，随行的只有 70 名勇敢的战士，出其不意地攻占了加里波利附近的茨姆佩城堡。正是在这里，土耳其人第一次在欧洲的土地上站稳了脚跟。拜占庭人将这群征服者比作波斯人，并以波斯人的名字称呼他们。但奥斯曼人比大流士和薛西斯的人民更可怕，也更幸运。如果按照波利比奥斯的说法，波斯人只要越过亚洲边界就注定要失败，那么土耳其人只是在踏上欧洲土地后才变得强大和伟大。

由于康塔库泽诺斯皇帝需要苏丹（他自己的女婿）的帮助，他不得不满足于对苏莱曼夺取色雷斯城市的微弱抗议。约翰五世-帕拉伊奥洛戈斯也遇到了同样的情况。1355 年，他成功夺取了君士坦丁堡，并消灭了他的对手。康塔库泽诺斯皇帝辞去了王位，在斯巴达结束了他动荡的修道生涯，他的知识分子儿子曼努埃尔根据与帕莱奥洛戈斯签订的条约，被允许继续作为暴君统治斯巴达。然而，拜占庭人对神学的关注几乎超过了对土耳其威胁的关注。他们的宗主教和皇帝在宗教会议、争论和著作中研究了泰伯山光明异象的本质。正如高卢主教萨尔维亚努斯曾因罗马人衰落的景象而说过，他们想笑着死去，就像被狞笑的草药所满足一样，一位谨慎的哲学家也可以这样评价当时的拜占庭人，他们想作为神学诡辩家死去。

没有什么能阻挡奥斯曼人在巴尔干半岛的前进，尤其是在强大的塞尔维亚统治者斯蒂芬-杜桑（Stephen Dusan）于 1355 年去世之后，这个伟大的斯拉夫国家在他的儿子乌罗斯五世（内马尼亚王朝的最后一个统治者）的统治下开始分崩离析。加里波利是色雷斯最重要的海上城市，当时还是欧亚之间的贸易大港，后来也被土耳其人控制，土耳其人成为整个切尔逊的主人。在此基础上，1359 年去世的奥尔汗之子穆拉德一世（Murad I）更加顺利地继承了父亲的征服事业。他攻克了著名的色雷斯大都会阿德里安堡，并于 1365 年取代亚洲的布鲁萨成为苏丹的治所，同时也成为奥斯曼帝国在欧洲的中心，只要它还没有成为君士坦丁堡这样的国际大都会，希腊皇帝就已经把自己限制在君士坦丁堡的领土内了。

穆拉德从色雷斯向西推进，一直到达巴尔干山口，向南进入风景优美的塞萨利。当土耳其军队穿过特莫比莱，逼近玻欧提亚和阿提卡时，敌军的抵抗丝毫没有阻挡他们前进的步伐。在这里，西西里政府的权力因内部动乱和加泰罗尼亚豪门之间爆发的争端而陷入瘫痪，一段时间以来，它不仅要抵御阿尔巴尼亚人和土耳其人的袭击，还要与希腊暴君米西特拉斯（Misithras）、阿尔戈斯的恩吉亚吉多（Guido of Enghia）以及威尼斯人交战。约翰内斯-德-劳里亚（Johannes de Lauria）于 1359 年 10 月 13 日以 "尊敬的底比斯人"（Commem.VI,fol.103）的身份证明了一项法令。当时，罗杰-德-劳里亚作为国王腓特烈三世的省督领导着公国。在反对派的压力下，同时又受到拜洛-内格罗蓬特的压力，他毫不犹豫地召唤即将到来的土耳其人来帮助他。作为他的盟友，土耳其人甚至进入了底比斯城--雅典公国的政府所在地和最负盛名的地方："Civitas nostra Thebarum, quae in ipsis ducatibus quasi caput est et magistra"，根据国王弗雷德里克三世的法令（巴勒莫档案馆，Reg. Protonot. I, a. 1349-93, fol. 108）。

这一事件证明，西班牙人和西西里人在希腊也是外国人，他们对希腊没有家的感觉。这一消息甚至在遥远的西方传播开来。乌尔班五世号召欧保亚领主、帕特雷大主教以及其他主教和统治者抵御威胁亚该亚的危险。Cum nuper audivimus, quod in civitate Thebarum et aliis circumstantibus partibus infidelium Turcorum profana multitudo moretur, ac terras fidelium principatus Achajae impugnare moleatur"。

他劝告塞浦路斯的侠义之王、卢西尼昂的彼得一世（Peter I of Lusignan）回国，因为他的国家也将面临异教徒的入侵。

1363 年 4 月 1 日，彼得在阿维尼翁与法国的约翰和萨瓦的阿玛迪奥六世一起承诺进行十字军东征。 由于没有得到欧洲列强的足够支持，他回到了塞浦路斯，然后对埃及进行了一次军事远征，结果只是在 1365 年 10 月 10 日征服并暂时占领了亚历山大城。

在此期间，底比斯大主教保罗、骑士巴托洛梅奥-德-瓦莱里伊斯、尼古拉斯-德-阿尔多伊诺和吉列尔姆-巴萨尼作为逃亡的底比斯人和雅典公国其他族群的使者来到西西里的腓特烈宫廷，他们向腓特烈报告了土耳其人占领雅典城的情况和国家的危急局势。1363 年 8 月，国王再次任命马特奥-德-蒙卡达为终身副主教，他拥有广泛的权力，甚至可以赦免国王的罪犯，并在他认为合适的时候任命城堡的城堡官和督军。由于他不仅向底比斯城，甚至还向内廷大臣罗杰-德-劳里亚（Roger de Lauria）报告了自己的任命，很显然，这位有影响力的人物既不会受到审判，也不会受到任何惩罚。Ind.（1363 年）；见 Ros. Gregorio, App. 65 我在巴勒莫档案中找到了这份文件，Reg. Prot. I, a. 1349-63, fol. 108，日期为 8 月 20 日，没有年份和征税诏告。然而，由于 8 月 16 日在锡拉库扎将博多尼察侯国（如果该侯国已被征服）授予同一位蒙卡达的日期是在第 109 卷，而在此之前的另一份文件上标有 Ind. I，因此 1363 年是肯定的。

罗杰并没有继续管理雅典公国，而是让蒙卡达继续在西西里为国王服务。1365 年 7 月，罗杰与威尼斯共和国谈判，以确认公司之前与海湾督军尼古拉-皮萨尼（Niccola Pisani）达成的二十年和平协议。然而，威权统治者只想承认其与内格罗蓬特首领多梅尼科-米歇尔（Domenico Michiel）最近达成的为期两年的停战协议。它还拒绝了罗杰关于允许公司自费装备舰队与敌人作战的要求。因此，威尼斯顽固地坚持阻止雅典的加泰罗尼亚人成为海军强国的同样条件："Quod suis expensis posset in mari armare contra suos inimicos"。- 威尼斯拒绝了这一说法，"quia nostrae intentionis est quod treugam predictam nuper factam per dictum nostrum bajulum in universitate inviolabiliter observetur"（Misti XXXI，第 108 卷，1365 年 7 月 25 日）。对信使罗杰斯的回复，他在此书中被称为 "marescalchus et vicarius generalis universitatis ducatus Athenarum"。

马迪奥-德-蒙卡达何时出现在公国不得而知。他成功地将底比斯从土耳其的入侵中解放出来，但并没有恢复该国的秩序，公司稳固的政治结构面临分崩离析的危险。大权独揽取代了法律。贵族加泰罗尼亚人彼得-德-普伊格（Peter de Puig，或称普伊格帕拉丁斯）是卡尔迪萨城堡和卡兰德里城堡的领主，似乎在蒙卡达缺席期间，他曾一度担任代理司祭，他得以在底比斯自立为暴君。在那里，他不仅将劳里亚家族赶出了他们的势力范围，还在与阿尔巴尼亚人的战争中，从阿拉贡纳家族的博尼法斯、胡安和杰米兄弟手中夺取了萨洛纳、利多里基和维特里尼察城堡。巴勒莫，Reg. Cancell. a. 1346, n. 4, fol. 127. 在该法案中，Petrus de Putheo（Puig）被明确命名为 "vicarius dictor. ducatuum"，因此缺少的 "generalis "并不是决定性的。普伊格家族可能与普伊格帕拉丁家族相同。参见文章： Die Lehen der Herzogtümer Athen und Neopaträ am Ende der catalanischen Herrschaft, Deltion der histor. 最后，以罗杰-德-劳里亚（Roger de Lauria）为首，在底比斯形成了一个反对篡位者的阴谋。彼得-德-普伊格（Peter de Puig）、他的妻子安吉丽娜（Angelina）和他最著名的几位支持者在起义中被杀，政府军遭到殴打。蒙卡达本人当时不在底比斯，罗杰和他的党羽在底比斯夺取了政权。他们派腓特烈国王作为他们的辛迪加和信使克雷莫纳的弗朗西斯（Francis of Cremona），1367 年 1 月 2 日，底比斯各城市的代表也曾向他发出委托书，为自己的这些过激行为称义，腓特烈三世出于无奈赦免了所有有罪的人。巴勒莫，Reg. Cancell. 该文件指出，授权书是在 "anno D. Incarn. 1366 secundo Jan. 印"。Wilh. de Almenara、Antonius de Lauria（罗杰之子）、Albertus de Bonacolsis of Mantua、Jakobus Guardia、Alfonsus Cavalerius、Bernardus Balestarius 等人在该专利中被列为罗杰的支持者。
