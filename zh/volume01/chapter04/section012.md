### 第四章

希腊与图像之争。希腊人反抗莱昂三世皇帝。他们在君士坦丁堡的失败。希腊的斯拉夫化进程。伯罗奔尼撒半岛的斯拉夫部落。斯拉夫人问题。古希腊没有出现斯拉夫帝国。阿提卡没有斯拉夫殖民地。雅典的艾琳成为希腊女皇。征服希腊的斯拉夫部落。雅典卫城成为国家监狱。在帕特雷击败斯拉夫人。雅典人提奥帕诺成为希腊女皇。

#### 1.

在康斯坦斯大帝昙花一现之后，雅典再次消失在没有历史的黑夜中。在相当长的一段时间里，这座被遗忘的城市没有一丝光亮。只有在伊索瑞王朝的建立者莱昂三世统治时期发生了著名的圣像争夺战，希腊才有了片刻的生机，从而再次出现在世人面前。

在东罗马帝国的历史上，自基督教传入以来，没有任何一场精神运动的重要性可以与圣像之争相比，这场圣像之争使拜占庭的国家、教会和社会动荡了一个多世纪。这场开明专制主义与教会迷信的殊死搏斗所产生的影响超出了伊索里亚人的预料，因为欧洲的巨大动荡与此有关：西方脱离拜占庭，教皇建立世俗统治，法兰克国王查理建立新帝国。

如果伊索里亚皇帝及其在部分高级神职人员和军队中的盟友大胆尝试从教会中去除偶像崇拜，将人民的道德和思想提升到更高的水平，并将国家从吸干它的修道院的多肉臂膀中解放出来，那么基督教共和国在8世纪就已经经历了一场改革，这场改革对人民的影响将使我们这个地区的整个发展呈现出不同的面貌。顺便提一下，反对教会形式的异教的改革斗争起源于小亚细亚和叙利亚，伊索里亚人就来自那里。是智力更加活跃的东方希腊主义提出了这一主张，而古希腊则变得不懂哲学，站在了反对改革的正统派一边。Paparrigopulos 的《地狱文明史》，巴黎，1878 年，第 IV 卷，强调了这些对比。

对图像的崇拜是基督教对异教诸神的造型和图像崇拜的蜕变；因此，希腊人尤其顽固地坚持这种崇拜是可以理解的，因为在这种崇拜中，他们所追求的艺术形式感得到了满足。对他们来说，教会取代了过去异教社会的辉煌：节日、戏剧、音乐、艺术和奥秘。即使拜占庭风格的救世主、圣母、天使和圣徒像见证了艺术在野蛮时代的衰落，希腊人的品味也同样低落。8 世纪的雅典人肯定会像他们的祖先欣赏菲迪亚斯的帕拉斯艺术品一样，虔诚地欣赏帕台农神庙中的穆西维亚雅典尼奥蒂萨像。雅典教会与古希腊的其他教会一样，是严格的正统教会，正如 Sathas, Bibl. graeca M. Aevi, IV, p. Aevi, IV, p. XXVII, Michael Psellos（显然是长者）的著作中的相关评论。它反对帝国的法令。虽然其他省份服从了这些法令，但早在 727 年，圣像禁令就迫使希腊人反抗莱昂皇帝。他们可能也是被拜占庭忽视的城市在贪婪的酋长手中遭受严重虐待所激怒的。从查士丁尼开始，希腊人和拜占庭人之间的对立不断加剧。拜占庭人在 8 世纪之前就已经摆脱了罗马主义和拉丁主义，他们的整个教会和政治社会都是希腊式的；然而，他们并不希望被视为希腊人，而是罗马人。也许没有什么比拜占庭人长达几个世纪的 "罗马人 "虚构更能清楚地表明古罗马的法律和国家观念在世界上打下了多么深刻的烙印。对他们来说，这一世界历史概念不可能具有民族意义，而只能具有政治意义。它表达了罗马帝国在君士坦丁堡（新罗马）的合法再现，君士坦丁将帝国的权力从旧凯撒城转移到了君士坦丁堡。奥斯托哥特人灭亡后，拜占庭将意大利和罗马作为不可分割的罗马帝国的行省进行统治，甚至在后来查理曼大帝重建西方帝国后，拜占庭统治者仍将自己视为罗马唯一合法的皇帝。因此，东方帝国仍然是罗马帝国、罗马尼亚，其臣民在法律上被称为罗马人。与中世纪的西方帝国不同，拜占庭帝国被随意称为罗马帝国，其臣民不被称为罗马人，而被称为 "Romaeans"。罗马尼亚"（意大利语：Romagna）这一名称从拜占庭本身传到拉文纳主教辖区，以便将这片留给希腊皇帝的意大利土地与那里的伦巴第行省区分开来。弗兰肯人也用 "罗马尼亚 "来称呼希腊。土耳其人称拜占庭帝国为 "Rûm"，他们保留了 "Rumeli "一词。对于所有拜占庭历史学家来说，希腊人都是 "罗马人"。直到 15 世纪，雅典人出身的 Laonicus Chalkokondylas 才再次称希腊人为 "希腊人"。这位历史学家在一段引人注目的文字中谈到了罗马人的名字被移到希腊的情况："罗马人取得世界霸权后，将罗马的管理权交给了他们的大祭司；皇帝（君士坦丁）亲自带领他们前往色雷斯；在那里，紧邻亚洲，他们将希腊城市拜占庭作为自己的大都会，并与步步紧逼的波斯人展开了战斗。希腊人与罗马人混杂在一起，但由于他们仍占多数，所以保留了自己的语言和民族；他们只是改变了自己的国名，因为拜占庭的皇帝们出于崇敬，希望被称为罗马人的皇帝，而不是希腊人的皇帝。Turcicis, ed. Bonn, L. I, p. 6。

从 7 世纪末开始，随着伊索里王朝的建立，教会、国家和社会进程取得了突破性进展，罗马拜占庭主义以君士坦丁堡为中心崛起，吞并了所有其他自治国家，希腊不得不进一步加深对拜占庭主义的反对。

非常奇怪的是，当谨慎的教皇格里高利二世（Gregory II）阻止意大利拜占庭各省因禁止图像崇拜而宣布废黜莱昂三世并建立一个信仰正统的新皇帝时，这个反叛计划的实施居然是被忽视的希腊人。这些人是拜占庭帝国的正统派；他们满脑子都是自己后裔的古老贵族意识，他们憎恨拜占庭人，认为他们是一群私生子，他们的皇帝本身就是伊索里亚野蛮人。毫无疑问，他们还与罗马有着更秘密的联系，对于教皇来说，没有什么比希腊人自己推翻他的帝国敌人更令人满意的了。他的管辖范围仍然包括马其顿和伊利里亚的主教区以及塞萨洛尼卡，希腊本土则包括首都科林斯。只是由于他激烈反对圣像破坏论的皇帝，罗马在帝国希腊各省的精神主权才被废除，教皇博尼法斯于 422 年任命他为帖撒罗尼迦主教。利奥一世任命他为自己的助理司祭，并命令伊利里亚的所有都主教服从他。格里高利一世向伊利里亚和亚该亚的主教发布命令。649 年，马丁一世将帖撒罗尼迦主教逐出教会。Jaffé, Reg. Pontif.

希腊人反抗莱昂三世的事件并没有确切地流传下来。我们只知道拜占庭人称希腊人（Helladics）和基克拉迪群岛的居民手持武器联合起来公开反抗。Theopan. I, p. 623: ‛Ελλαδικοί τε καὶ τω̃ν Κυκλάδων νήσων, 帕拉丁编年史作者说这些反抗者充满了神圣的狂热，即对图像的崇拜。Kedrenos I, p. 796 他们装备了一支舰队，让斯蒂法诺斯和图尔马琴-阿盖利亚诺斯（Turmarchen Agellianos）担任舰长，带着一个野心家科斯马斯（Kosmas）驶向君士坦丁堡，科斯马斯当然是希腊民族，他们想把他提升为东正教皇帝。直到 727 年 4 月 18 日在首都城墙外的一场海战中，叛军舰队才被希腊人的炮火摧毁。阿盖利亚诺斯绝望地投海自尽，科斯马斯和斯蒂法诺斯的头颅也被刽子手砍下。

拜占庭历史学家没有意识到民族起义的失败给古希腊带来了怎样的后果。因为起义的原因之一必须被视为帝国禁止图像崇拜，而且在起义者被消灭后，莱昂三世和他热情洋溢的儿子君士坦丁试图在帝国全境执行他们的法令，对起义的神职人员进行严厉迫害，希腊也必然被基督教偶像崇拜的废墟所覆盖。因此，人们认为希腊古代艺术品的最后残余也在这些圣像破坏者的屠杀中消亡了。然而，圣像破坏者的毁灭怒火不可能针对异教徒的崇拜图像，因为这些图像早已变得无害，它们作为公共珠宝装饰着君士坦丁堡和帝国的其他城市，并在那里保存了几个世纪。科迪诺斯提到伊索里安人莱昂摧毁了许多古老的雕像（θεάματα αρχαι̃α），他指的是基督教的雕像，因为他提到，除其他事项外，皇帝还推翻了君士坦丁大帝在查尔克树立的救世主雕像，后来女皇艾琳用一尊 Musiv 取代了它、 只要教堂和修道院里有木雕、石雕和金属雕刻的圣人像，它们就会被毁坏，但相对于修道士从他们的作坊和工厂向教堂提供的彩绘圣人像而言，这些圣人像的数量肯定很少。因此，圣像破坏者的愤怒主要针对上述圣人像，甚至连教堂里的马赛克和壁画也用石灰粉刷过。

由于图书馆和学校与修道院紧密相连，其中一些可能在圣像破坏中被毁。然而，将拜占庭的艺术和科学在野蛮中消亡归咎于圣像破坏者是错误的。伊索里亚人绝不是粗鲁无知的人。艺术并没有在拜占庭消亡，早在 9 世纪，圣像破坏运动带来的思想动荡就在君士坦丁堡催生了科学的重生，当时，一位对缪斯友好的赞助人凯撒-巴尔达斯在马格诺拉宫创办了一所新的学院，其负责人是塞萨洛尼卡的莱昂大主教，博学的普提欧斯就出自该学院。

我们不知道雅典城在多大程度上参与了希腊起义。然而，727 年的奇怪起义似乎是古希腊民族意识的突然觉醒。这也可以证明，希腊大陆和萨摩斯岛周围岛屿上的希腊人已经崛起为相对强大的力量。因为在 8 世纪前三分之一的时间里，希腊仍有人口众多、繁荣昌盛的城市，足以用自己的资源装备海军，进行政治革命。因此，斯拉夫人还不可能使希腊民族变得无能为力，甚至将其吞噬。
