### 第四章

伊庇鲁斯的西奥多征服了帖撒罗尼迦。保加利亚沙皇约翰-阿森二世。鲍德温二世的摄政王约翰。维勒哈杜安保卫拜占庭，对抗约翰-阿森和巴特兹。希腊的拉丁封建贵族。亚该亚的男爵。雅典领主的居住地底比斯。热那亚人在底比斯和雅典的定居。维勒哈杜因的威廉二世，亚该亚王子。他征服了拉科尼亚，并建造了米西特拉城堡。欧波亚的关系。Dalle Carceri 家族的三位领主。威尼斯获得他们的主权。维勒哈杜安（Villehardouin）声称拥有欧博瓦和雅典的主权。威尼斯人、欧波亚人和希腊的男爵们结盟反对这位王子。

#### 1.

甚至在梅加斯基尔人离开希腊之前，在特莫比莱之外就发生了一些事件，这些事件也对雅典国产生了重要影响。1222 年，伦巴第王国塞萨洛尼卡被埃皮罗斯的暴君西奥多-安杰洛斯所灭，该王国年轻的王子德梅特里奥斯（博尼法斯之子）娶了奥托-德拉罗什的侄女为妻。这也结束了雅典国家对该王国最后的封建依附。帖撒罗尼迦的陷落吓坏了弗兰肯人，但不在西方的德米特里厄斯向教皇求助却徒劳无功，1225 年 2 月 12 日，教皇向罗马尼亚的全体神职人员发出训诫："Ne capta Thessalonica desperent"。他的同父异母兄弟蒙费拉托藩侯威廉四世要么太聪明，要么太无能，没有能力进行海外冒险。他放弃了父亲曾经的领地塞萨洛尼卡，因此一位吟游诗人愤怒地对他喊道，他不像罗伯特-吉斯卡尔的儿子那样征服了安提阿和蒙吉扎尔特，他似乎是个私生子，不配成为西托的修道院院长、 诗人认为征服帖撒罗尼迦是如此容易，以至于侯爵不需要弹弓或导弹。

西奥多在帖撒罗尼迦称帝后，一个希腊民族帝国似乎在他的权杖下重新建立起来，因为安杰洛伊人的统治现在从杜拉佐延伸到了帖撒利海岸。伊庇鲁斯帝国打断了君士坦丁堡的拉丁人与希腊的法兰克封建国家之间的联系；如果它继续存在，也会粉碎他们。但是，在巴尔干半岛形成的所有斯拉夫和希腊国家的重心都不在希腊，而是在博斯普鲁斯海峡，它们的目标既不是雅典也不是科林斯，而是君士坦丁堡这座国际大都市，这一事实消除了危险。在尼卡亚，他同样精力充沛的女婿约翰-巴特泽斯（John Batatzes）在光荣的拉斯卡里斯（Laskaris）于 1222 年去世后登上了王位。

因此，当时在旧康美尼亚帝国的废墟上站着三位皇帝：博斯普鲁斯海峡上软弱的拉丁皇帝和他的两个强大对手，即帖撒罗尼迦和尼西亚的希腊统治者，他们都希望成为君士坦丁堡的合法继承人。如果这两个人真诚地结盟，他们很可能会摧毁拉丁帝国。但嫉妒使他们成为了竞争对手。此外，还有第四股势力也不容忽视。自 1218 年以来，一位名叫约翰-阿森二世的伟人一直坐在特尔诺沃的保加利亚王座上，忙于实施建立一个以君士坦丁堡为首都的伊利里亚帝国的大胆计划。在彼得大帝之前的几个世纪，拥有君士坦丁堡是斯拉夫王公们的梦想。

1230 年，保加利亚沙皇沉重打击了埃皮罗斯，后来允许被囚禁、双目失明的西奥多继续在帖撒罗尼迦称帝，因为他自己也爱上了他的女儿艾琳，并娶她为妻。尼西亚皇帝现在面对的是作为帝国请求者的保加利亚统治者，但这些王位请求者同意联手将拉丁人赶出君士坦丁堡。鲍德温二世是考特奈的罗伯特的不肖子孙和继承人，他于 1228 年去世，统治那里的是一位八十多年的英雄，即耶路撒冷名义上的国王、霍恩斯托芬王朝腓特烈二世的岳父、同时也是他在教皇麾下的对手--布里安的约翰。法兰克男爵们将他从意大利召来担任年轻王子的监护人，布里安的约翰于1231年来到君士坦丁堡，在圣索菲亚教堂加冕称帝。

面对强敌的进攻，他通过巧妙的谈判，而不是凭借日渐衰落的拉丁帝国的微弱力量，艰难地抵御住了敌人的进攻。在教皇诏书的支持下，他号召所有封臣保卫被围困的首都。其中，最有实力和最有意愿的是亚该亚王子戈特弗里德二世。维尔哈杜安。他承诺每年补贴 22000 金币，并组织了一支军队。梅加斯基尔地区的教会按照教皇的命令缴纳了战争什一税。1236 年，当保加利亚沙皇和巴特泽斯皇帝的军队从海上和陆地围攻君士坦丁堡时，是亚该亚王子拯救了这座城市。他率领自己的舰队，在威尼斯桨帆船的配合下，攻入金角湾，驱散敌船，迫使围攻者离开。雅典的圭多（Guido of Athens）是否也参与了这一军事行动，尚无定论。

这样，摇摇欲坠的拉丁帝国又获得了短暂的喘息机会。1237 年，布里埃纳的约翰死后，鲍德温二世得以登上拜占庭的王位，他从西域回国，乞求保护。幸运的是，他的敌人联盟解体了。约翰-阿森二世于 1241 年去世，鲍德温二世与其继任者科洛曼缔结了休战协议，约翰-巴特泽斯也加入了该协议。保加利亚帝国的势力很快瓦解，巴尔干斯拉夫人也证明他们无法在阿桑王朝的统治下建立一个持久的国家。只有个别大胆的人所产生的野蛮冲动才使这些民族开始行动，并一度使他们变得暴力而可怕。他们从未形成稳固的政治形式。

因此，希腊本土的法兰克诸国与威尼斯人联合起来，用行动表明，他们认识到维护拜占庭的拉丁帝国是一种责任，即使不是他们与之结成封建联盟的责任，至少也是他们的利益。然而，从根本上说，他们自身的存在不再取决于君士坦丁堡的命运，因为他们已经脱离了君士坦丁堡，过上了独立的政治生活。13 世纪的拉丁封建制度和骑士社会在奥埃塔以南的土地上生根发芽，其表面呈现出法国风情。正如教皇奥诺留三世所言，一个新的法兰西出现在佩内奥斯河、阿尔卑斯河、欧罗塔斯河和伊利索斯河畔。这种西方殖民化虽然在希腊人中仍然是独特和封闭的，但却能够得到发展。如果说希腊的法国人和意大利人，就像意大利的哥特人和伦巴第人或高卢的弗兰肯人一样，没有被外来的民族因素所吸收，那只是因为他们比哥特人和伦巴第人有更强大、更自信的个性，在道德上也更先进，最后还因为他们的种族因同胞的涌入而得以保持，而伟大的拉丁教会却将他们与希腊人永远隔开了。

东方希腊人和西方人之间没有选择性的亲缘关系，因此没有融合。希腊人永远无法拉丁化，他们的语言、宗教和教育也永远无法根除。就在法国人和意大利人在希腊建国的同时，德意志骑士团自 1230 年起就在欧洲东北部建立了殖民地。该骑士团还在希腊，特别是在莫雷亚获得了土地。1209 年，在安德拉维达分配骑士和神职人员的领地时，德意志兄弟会获得了卡拉马塔和莫斯滕尼萨城堡的四块领地，罗马尼亚的指挥官就居住在那里。与马耳他骑士团和圣殿骑士团相比，德意志骑士团更为幸运，它在从东方到普鲁士的希腊法兰克国家的崩溃中保存了自己。马林堡在那里的地位就像安德拉维达在伯罗奔尼撒和雅典在阿提卡一样。在普鲁士和立陶宛的荒原上，这个骑士团成功地完成了弗兰肯人在开垦的希腊土地上无法完成的任务，形成了具有强大生命力的政治创造，以至于在半个多世纪之后，它成为德意志民族帝国重组的最重要因素之一。

弗兰肯人在希腊的统治并没有对希腊进行改造，他们最终也没有在希腊文化上留下持久的印记。虽然希腊的阳光使这些征服者的举止变得柔和，但他们几乎没有意识到，由于巨大的命运，他们成为了这个国家的主人，而这个国家所产生的杰出作品和思想比地球上任何其他民族都要多。世界历史的这希腊一页不是为他们而写的，也不是永远翻过去的。雅典、斯巴达、底比斯和科林斯激起了他们所有不那么理想的情感，因为这些城市本身早已衰败，居住着一个沉没的民族，他们在很大程度上忘记了自己的过去，希腊的古典废墟来自一个 "诺丁格里斯 "的时代。

从来没有哪个民族像希腊的弗兰肯人一样，在古老的土地上保持着现代性。即使是叙利亚十字军中最粗鲁的士兵，作为基督徒也能理解耶路撒冷对人类的重要性，但对于雅典和斯巴达，即使是拉罗什和维尔哈杜安也没有理解的钥匙。为了拥有这样一把钥匙，他们首先必须了解什么是艺术美和科学，什么是希腊语，荷马、菲迪亚斯、索福克勒斯、品达和柏拉图这些名字意味着什么。几个世纪过去了，君士坦丁堡变成了土耳其人的地盘，雅典在西方的存在被遗忘，然后又被重新发现，13 世纪拉丁人的曾孙们才重新出现在希腊，满心欢喜地寻找古希腊世界被掩埋的每一个痕迹，同时揭开了法兰克人统治希腊的历史，除了一些城市和城堡的名字之外，这些历史也被遗忘了。

许多封建城堡的遗址，尤其是伯罗奔尼撒半岛上的城堡遗址，见证了拉丁贵族的铁血精神和骑士风范。帕特雷（Patrae）的阿勒曼尼（Alemanni）家族、梅萨雷亚（Arcadia）阿科瓦（Akova）或马特格里丰（Mategriffon）的罗齐埃（Rozieres）家族、旧戈尔蒂斯（Skorta）卡里特纳（Karytena）的布鲁耶（Bruyeres）家族、阿卡迪亚卡拉夫里塔（Kalavrita）的图尔奈（Tournay）家族、沃斯蒂察（Vostitsa）的夏尔皮尼（Charpigny）家族、阿戈利斯（Argolis）维利戈斯提（Veligosti）和达马拉（Damala）的比利时瓦兰库尔（Valaincourt）家族、帕萨瓦（Passava）的诺伊（Neuilly）家族，作为王子的伴侣，使阿卡亚的城堡充满了喧闹。 布琼（Buchon）和霍普夫（Hopf）将这些男爵府放在一起，最近甚至有一位女士也试图这样做： Diane de Guldencrone，L'Achaïe féodale，巴黎，1886 年 - 参见 Ch. A. Leving，La Principauté d'Achaïe et de Morée，布鲁塞尔，1879 年。 Gottfried II Villehardouin 的王宫有 700 到 1000 名骑士，即使在西方也被认为是最好的习俗学校。埃利斯（Elis）的安德拉维达（Andravida）和附近位于赞特（Zante）对面的切洛纳塔（Chelonata）岬角上的克拉伦扎（Clarenza）港口，都是亚该亚统治者的所在地。在这个岬角上矗立着强大的 Chlemutsi 或 Clermont 城堡，也被称为 Tornese 城堡，由戈特弗里德二世建造，目的是保护港口，因为自 1250 年以来，希腊各地广泛使用的亚该亚硬币 "deniers tournois "就是在这里铸造的。 Schlumberger, Numismatique de l'Orient latin, Paris 1878, p. 130. Leake, Peloponnesiaca, p. 210。210 在古埃利斯的废墟上耸立着新的庞蒂科斯城堡（Pontikos）或贝尔维德尔城堡（Belvedere），从这里可以俯瞰埃托利亚（Aetolia）海岸、扎金索斯岛（Zakynthos）、凯普哈伦尼亚岛（Kephallenia）和伊萨卡岛（Ithaca），内陆则是佩内奥斯河（Peneios）的绿色草地，一直延伸到东北部的埃里曼西亚山林和拉顿河（Ladon）流经奥林匹亚北部的山脉。

在底比斯和雅典，梅加斯基尔人的宅邸不如维尔哈杜安人的宅邸富丽堂皇。由于阿提卡是一个贫穷的国家，其首都又是一个偏远的非中心地带，圭多一世更愿意将他的居所设在底比斯，莫雷亚编年史多次提到底比斯是他的居所。卡德摩斯城位于肥沃的波欧提亚，与法兰克的欧波亚、希腊北部和亚该亚公国的联系更为密切。这里空气清新，水源丰富，周围环绕着备受赞誉的迪尔克河、阿雷索萨河、埃皮克雷尼河和伊斯梅诺斯河。第一任梅加斯基尔已经将底比斯的一半作为封地赐给了他的侄子吉多-德拉罗什，但他又将另一半赐给了自己的妹妹邦妮，邦妮将其作为嫁妆嫁给了她的丈夫贝拉，贝拉是圣奥梅尔的雅克的儿子。这个佛兰德家族就这样在底比斯站稳了脚跟，并得到了那里一半的领地和八个骑士封地。

可以肯定地说，拜占庭战略家以前居住的卡德梅亚城堡很容易就会成为梅加斯基尔的居所。宙斯-希普西斯托斯、提切、阿佛洛狄忒和得墨忒耳的古老神庙早已成为废墟，它们的材料被拜占庭人用来建造新的住所和防御工事。不过，在 13 世纪，巨大的古城墙保留下来的部分肯定比现在要多。也许下环墙的七座城门仍然可以辨认出来。保萨尼亚斯还见过它们，但在他那个时代，底比斯下城已经荒废，只有卡德梅亚的一排长长的山丘上有人居住，山丘下是肥沃的平原，一直延伸到科帕伊塞，那里有一座红色的岩石山，保留着狮身人面像的记忆。Bursian, Geogr. Griech., I, 225。

根据 Tzetzes（编辑：Kiesling）在 Histor. Var. chil. X, v. 389 ff.

> Φύσει τω̃ν σφω̃ν υδάτων  
Διαύγειαν καὶ στίλψιν δὲ καὶ γε πολὺ τὸ λει̃ον  
Niketas, De Alexio, p. 609: άπερ εκ Θηβω̃ν επταπύλων βασιλει̃ν κεχορήγηται. 1218 年左右访问底比斯的安达卢西亚犹太马卡门诗人查里斯肯定没有给那里的希伯来诗人留下好印象。Hopf I，第 164 页。

热那亚商人在底比斯和雅典定居，试图将威尼斯人赶出当地市场。热那亚与底比斯的贸易关系尤其要早于法兰克人的征服，1169 年左右，一份给热那亚驻拜占庭使节的指示要求热那亚在底比斯和雅典开展贸易。1169 年，一份给热那亚使节给拜占庭皇帝的指示指出，他应该设法从拜占庭皇帝那里获得整个帝国的贸易自由，特别是允许他进行 "apud Stivam sicut Veneti soliti erant"（"apud Stivam sicut Veneti soliti erant"）的丝绸贸易；Giornale Ligustico di Arch. 因为在 1240 年 12 月 24 日，他给予热那亚人在他的各州以及底比斯和雅典的安全居住权、贸易特权、免税（在梅加斯基尔织造的丝织品的出口税除外）以及他们自己的民事管辖权。这份特许状证明，热那亚人在 1240 年之前就已经与雅典国的一位执政官达成了协议。DCCLVII 载于 Liber jurium Rep. Genuensis，都灵 1854 年；日期为 1240 年 12 月 23 日，Riccius de Sancto Donato 作为热那亚执政官主持。从那时起，这些碑文就一直保存在城市中；即使在两个世纪后的雅典，这些碑文依然清晰可见，忒修斯神庙的第七根柱子上还刻有碑文： Vit. Conzadus Spinula 1453 die 20 yanuazyo. Const. Zesios (Deltion der hist. u. ethnol. Gesell. II, 1885, p. 23) 可能将 z 读成了 r，错误地认为 Spinula 是西班牙人。当然，他们的定居并不排除其他来自西方和黎凡特的商人。I 278 年，威尼斯共和国要求对被拜占庭海军上将抢劫的乔治-德尔菲诺（Georgio Delfino）进行赔偿。德尔菲诺乘坐拜占庭海军上将的驳船从雅典驶往内格罗蓬特时被其抢劫。不过，这位威尼斯人被描述为 "abitator Setine"（雅典）。Tafel and Thomas III，第 178 页。

在他的国家里，希腊人已经认命，和平没有受到干扰，这使得大西西里人 Guido 有可能负责波欧提亚和阿提卡的贸易和农业发展。由于征服伯罗奔尼撒半岛的工作尚未完成，他只需参加亚该亚不安分的王子必须进行的军事行动。1245 年戈特弗里德二世去世后，他的弟弟威廉，一个具有骑士精神和强大意志力的人，开始统治那里。根据马林-萨努多-托塞洛（Marin Sanudo Torsello）在《罗马尼亚共和国史》（Istoria del Regno di Romania）第 101 页中的记载，威廉有三个来自香槟区的侄女投靠了他；她们的丈夫分别是雅典的圭多、萨洛纳的托马斯二世和欧波亚的三等公爵古列尔莫-达-维罗纳（Gulielmo da Verona）。他不仅以朋友的身份，还以阿尔戈斯和瑙普利亚封臣的身份，支持他对抗蒙巴萨的努力。这座被认为是坚不可摧的希腊自由城市，伯罗奔尼撒半岛的直布罗陀，最终在 1248 年经过长期围攻后投降，亚该亚王子现在才称得上是整个半岛的主人。因为 Taygetos 的斯拉夫人部落很快也臣服于他。在古斯巴达遗址上，距其废墟三英里处建造了伟大的米西特拉城堡，其名称似乎是希腊语而非斯拉夫语。 希腊语名称为 Μιζιθρα̃（《莫里亚希腊编年史》）。前拜占庭都城拉塞岱蒙成为科林斯的附属主教区。

在帖撒罗尼迦王国灭亡、君士坦丁堡的拉丁帝国彻底衰落的时候，这些巨大的成功助长了王子的野心，他试图将自己的统治扩展到整个希腊。蒙费拉特的博尼法斯未能实现的目标，他现在想要实现。他雄心勃勃的想法导致了他与雅典领主之间的裂痕，而这是由欧波亚的情况引起的。
